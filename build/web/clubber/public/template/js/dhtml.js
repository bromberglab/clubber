var MSIE, Netscape, Opera, Safari, Firefox;

if(window.navigator.appName.indexOf("Internet Explorer") >= 0){
    MSIE = true;
}else if(window.navigator.appName == "Opera"){
    Opera = true;
}else if(window.navigator.userAgent.indexOf("Safari") >= 0){
    Safari = true;
}else if(window.navigator.userAgent.indexOf("Firefox") >= 0){
    Firefox = true;
    Netscape = true;
}else{
    Netscape = true;
}

//-------------------------------------------------------------
// ¥³¥ó¥¹¥È¥é¥¯¥¿
//-------------------------------------------------------------
function Component(id)
{
    this._component = document.getElementById(id);
    this._opacity_change_interval = 1;
    
    var opc = this._component.style.opacity;
    if(opc == "")
    {
        opc = 1;
    }
    this._opacity = opc * 100;
}


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤Îid¤ò¼èÆÀ¤¹¤ë¥á¥½¥Ã¥É
//-------------------------------------------------------------
function _Component_ID()
{
    return this._component.id;
}
Component.prototype.id = _Component_ID;

function _Component_FontSize(size)
{
    if(_defined(size))
    {
        this._component.style.fontSize = size + "px";
    }
    else
    {
        return this._component.style.fontSize;
    }
}
Component.prototype.fontSize = _Component_FontSize;


//--------------------------------------------------------------
// É½¼¨¡¦ÈóÉ½¼¨¤Î»þ¤Ë½ù¡¹¤ËÆ©ÌÀÅÙ¤òÊÑ¤¨¤Æ¤¤¤¯´Ö³Ö¡Ê»þ´Ö¡Ë¤ò
// ÀßÄê¤¹¤ë¡£ÃÍ¤¬¾®¤µ¤¤¤Û¤ÉÂ®¤¯ÊÑ²½¤¹¤ë¡£
//--------------------------------------------------------------
function _Component_OpacityChangeInterval(interval)
{
    if(typeof(interval) == "undefined")
    {
        return this._opacity_change_interval;
    }
    else
    {
        this._opacity_change_interval = interval;
    }
}
Component.prototype.opacityChangeInterval = _Component_OpacityChangeInterval


//--------------------------------------------------------------
// ÆâÉô¤ËÉ½¼¨¤¹¤ëHTML¤òÊÑ¹¹¤¹¤ë¥á¥½¥Ã¥É
//--------------------------------------------------------------
function _Component_HTML(html)
{
    var component = this._component;
    
    if(typeof(html) == "undefined")
    {
        return component.innerHTML;
    }
    else
    {
        component.innerHTML = html;
    }
}

Component.prototype.HTML = _Component_HTML;

//----------------------------------------------------------
// ÇØ·Ê¿§ÊÑ¹¹¥á¥½¥Ã¥É
//----------------------------------------------------------
function _Component_BackgroundColor(color)
{
    this._component.style.backgroundColor = color;
}
Component.prototype.backgroundColor = _Component_BackgroundColor;


//----------------------------------------------------------
// ¾å¤Î¥Ü¡¼¥À¡¼¤Î¥¹¥¿¥¤¥ëÀßÄê¤ò¤¹¤ë¡£
//----------------------------------------------------------
function _Component_BorderTop(border)
{
    if(_defined(border)){
        var comp = this._component;
        if(MSIE)
        {
            //comp.style.borderTop = border.color();
            //comp.style.border-top-style = border.style();
            //comp.style.border-top-width = border.width();
        }
        else
        {
            comp.style.borderTopColor = border.color();
            comp.style.borderTopStyle = border.style();
            comp.style.borderTopWidth = border.width() + "px";
        }
    }
}
Component.prototype.borderTop = _Component_BorderTop;

//----------------------------------------------------------
// ²¼¤Î¥Ü¡¼¥À¡¼¤Î¥¹¥¿¥¤¥ëÀßÄê¤ò¤¹¤ë¡£
//----------------------------------------------------------
function _Component_BorderBottom(border)
{
    if(_defined(border)){
        var comp = this._component;
        if(MSIE)
        {
        }
        else
        {
            comp.style.borderBottomColor = border.color();
            comp.style.borderBottomStyle = border.style();
            comp.style.borderBottomWidth = border.width() + "px";
        }
    }
}
Component.prototype.borderBottom = _Component_BorderBottom;

//----------------------------------------------------------
// º¸¤Î¥Ü¡¼¥À¡¼¤Î¥¹¥¿¥¤¥ëÀßÄê¤ò¤¹¤ë¡£
//----------------------------------------------------------
function _Component_BorderLeft(border)
{
    if(_defined(border)){
        var comp = this._component;
        if(MSIE)
        {
        }
        else
        {
            comp.style.borderLeftColor = border.color();
            comp.style.borderLeftStyle = border.style();
            comp.style.borderLeftWidth = border.width() + "px";
        }
    }
}
Component.prototype.borderLeft = _Component_BorderLeft;

//----------------------------------------------------------
// ±¦¤Î¥Ü¡¼¥À¡¼¤Î¥¹¥¿¥¤¥ëÀßÄê¤ò¤¹¤ë¡£
//----------------------------------------------------------
function _Component_BorderRight(border)
{
    if(_defined(border)){
        var comp = this._component;
        if(MSIE)
        {
        }
        else
        {
            comp.style.borderRightColor = border.color();
            comp.style.borderRightStyle = border.style();
            comp.style.borderRightWidth = border.width() + "px";
        }
    }
}
Component.prototype.borderRight = _Component_BorderRight;


//----------------------------------------------------------
// ¾å²¼º¸±¦¤Î¥Ü¡¼¥À¡¼¥¹¥¿¥¤¥ë¤ò°ì³ç¤ÇÀßÄê¤¹¤ë¡£
// °ú¿ô¤Î¸Ä¿ô¤ÇÀßÄê¤¹¤ë²Õ½ê¤¬ÊÑ¤ï¤ë¡£
//
// °ú¿ô1¸Ä¡§¾å²¼º¸±¦Æ±¤¸ÀßÄê
// °ú¿ô2¸Ä¡§»ØÄê¤·¤¿½ç¤Ë "¾å²¼" "º¸±¦" ¤òÀßÄê
// °ú¿ô3¸Ä¡§»ØÄê¤·¤¿½ç¤Ë "¾å" "º¸±¦" "²¼" ¤òÀßÄê
// °ú¿ô4¸Ä¡§»ØÄê¤·¤¿½ç¤Ë "¾å" "±¦" "²¼" "º¸" ¤òÀßÄê
//----------------------------------------------------------
function _Component_Border()
{
    var arg = _Component_Border.arguments;
    
    if(arg.length == 1)
    {
        this.borderTop(arg[0]);
        this.borderBottom(arg[0]);
        this.borderLeft(arg[0]);
        this.borderRight(arg[0]);
        
    }
    else if(arg.length == 2)
    {
        this.borderTop(arg[0]);
        this.borderBottom(arg[0]);
        this.borderLeft(arg[1]);
        this.borderRight(arg[1]);
        
    }else if(arg.length == 3)
    {
        this.borderTop(arg[0]);
        this.borderLeft(arg[1]);
        this.borderRight(arg[1]);
        this.borderBottom(arg[2]);
        
    }
    else if(arg.length == 4)
    {
        this.borderTop(arg[0]);
        this.borderRight(arg[1]);
        this.borderBottom(arg[2]);
        this.borderLeft(arg[3]);
    }
}
Component.prototype.border = _Component_Border;


//----------------------------------------------------------
// X¼´Êý¸þ¤ÎºÂÉ¸»ØÄê¥á¥½¥Ã¥É
// °ú¿ô¤òÅÏ¤µ¤Ê¤±¤ì¤ÐX¼´Êý¸þ¤ÎºÂÉ¸¤òÊÖ¤¹¡£
//----------------------------------------------------------
function _Component_X(x)
{
    var component = this._component;
    
    if(typeof(x) == "undefined")
    {
        var ret = (MSIE) ? component.style.pixelLeft : parseInt(component.style.left);
        return ret;
    }
    else
    {
        if(MSIE)
        {
            component.style.pixelLeft = x;
        }
        else if(Opera)
        {
            component.style.left = x;
        }
        else
        {
            component.style.left = x + "px";
        }
    }
}
Component.prototype.x = _Component_X;


//-------------------------------------------------------------
// Y¼´Êý¸þ¤ÎºÂÉ¸»ØÄê¥á¥½¥Ã¥É
// °ú¿ô¤òÅÏ¤µ¤Ê¤±¤ì¤ÐY¼´Êý¸þ¤ÎºÂÉ¸¤òÊÖ¤¹¡£
//-------------------------------------------------------------
function _Component_Y(y)
{
    var component = this._component;
    
    if(typeof(y) == "undefined")
    {
        var ret = (MSIE) ? component.style.pixelTop : parseInt(component.style.top);
        return ret;
    }else
    {
        if(MSIE)
        {
            component.style.pixelTop = y;
        }
        else if(Opera)
        {
            component.style.top = y;
        }
        else
        {
            component.style.top = y + "px";
        }
    }
}
Component.prototype.y = _Component_Y;


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤Î°ÜÆ°¥á¥½¥Ã¥É
// °Ê²¼¤ÎÊýË¡¤È°ÕÌ£¤ÏÆ±¤¸¡£
//
//   var component = new Component(id);
//   component.x(x);
//   component.y(y);
//-------------------------------------------------------------
function _Component_Move(x, y)
{
    this.x(x);
    this.y(y);
}
Component.prototype.move = _Component_Move;


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤ÎÉý»ØÄê¥á¥½¥Ã¥É¡£
// °ú¿ô¤ò»ØÄê¤·¤Ê¤±¤ì¤Ð¸½ºß¤ÎÉý¤òÊÖ¤¹¡£
//-------------------------------------------------------------
function _Component_Width(width)
{
    var component = this._component;
    
    if(typeof(width) == "undefined")
    {
        var ret = (MSIE) ? component.style.pixelWidth : parseInt(component.style.width);
        return ret;
    }
    else
    {
        if(MSIE)
        {
            component.style.pixelWidth = width;
        }
        else if(Opera)
        {
            component.style.width = width;
        }
        else
        {
            component.style.width = width + "px";
        }
    }
}
Component.prototype.width = _Component_Width;


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤Î¹â¤µ»ØÄê¥á¥½¥Ã¥É¡£
// °ú¿ô¤ò»ØÄê¤·¤Ê¤±¤ì¤Ð¸½ºß¤Î¹â¤µ¤òÊÖ¤¹¡£
//-------------------------------------------------------------
function _Component_Height(height)
{
    var component = this._component;
    
    if(typeof(height) == "undefined")
    {
        var ret = (MSIE) ? component.style.pixelWidth : parseInt(component.style.width);
        return ret;
    }
    else
    {
        if(MSIE)
        {
            component.style.pixelHeight = height;
        }
        else if(Opera)
        {
            component.style.height = height;
        }
        else
        {
            component.style.height = height + "px";
        }
    }
}
Component.prototype.height = _Component_Height;


//-------------------------------------------------------------
// ¥µ¥¤¥ºÊÑ¹¹¥á¥½¥Ã¥É¡£
// °Ê²¼¤ÈÆ±¤¸¡£
//
//   var component = new Component(id)
//   component.width(width);
//   component.height(height);
//-------------------------------------------------------------
function _Component_Size(width, height)
{
    this.width(width);
    this.height(height);
}
Component.prototype.size = _Component_Size;


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤ÎÉ½¼¨¡¢ÈóÉ½¼¨¤òÀÚ¤êÂØ¤¨¤ë¡£
// °ú¿ô¤ò»ØÄê¤·¤Ê¤«¤Ã¤¿¾ì¹ç¤Ï¡¢¸½ºß¤ÎÉ½¼¨¾õÂÖ¤òÊÖ¤¹¡£
//
// °ú¿ô :
//   visible : É½¼¨¡¢ÈóÉ½¼¨¤Î»ØÄê (boolean)
//-------------------------------------------------------------
function _Component_Visible(visible)
{
    var component = this._component;
    
    if(typeof(visible) == "undefined")
    {
        return (component.style.visibility == "visible") ? true : false;
    }
    else
    {
        if(MSIE || Safari || Firefox || Opera)
        {
            if(visible)
            {
                component.style.visibility = "visible";
                this._opacityStep = 10;
                this._opacity = 0;
            }
            else
            {
                this._opacityStep = -10;
                this._opacity = this.opacity();
            }
            
            _addComponent(this);
            
            this.changeOpacity();
        }
        else
        {
            component.style.visibility = (visible) ? "visible" : "hidden";
        }
    }
}
Component.prototype.visible = _Component_Visible;


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤ÎÆ©ÌÀÅÙ¤ò½ù¡¹¤ËÊÑ¹¹¤¹¤ë¥á¥½¥Ã¥É¡£
//-------------------------------------------------------------
function _Component_ChangeOpacity()
{
    var opacity = this._opacity + this._opacityStep;
    
    this.opacity(opacity);
    
    if(opacity >= 100)
    {
        return;
    }
    else if(opacity <= 0)
    {
        this._component.style.visibility = "hidden";
        return
    }
    else
    {
        var interval = this._opacity_change_interval;
        setTimeout("_triggerChangeOpacity('" + this.id() + "')", interval);
    }
}
Component.prototype.changeOpacity = _Component_ChangeOpacity;


//-------------------------------------------------------------
// ¥ì¥¤¥ä¤ÎÆ©ÌÀÅÙ¤ò»ØÄê¤·¤¿ÃÍ¤ËÊÑ¹¹¤¹¤ë¥á¥½¥Ã¥É¡£
//-------------------------------------------------------------
function _Component_Opacity(opacity)
{
    if(typeof(opacity) == "undefined")
    {
        return this._opacity;
    }
    else
    {
        this._opacity = opacity;
        
        var component = this._component;
        component.style.opacity = opacity / 100;
        component.style.mozOpacity = opacity / 100;
        component.style.filter = "alpha(opacity=" + opacity + ")";
    }
}
Component.prototype.opacity = _Component_Opacity;

//----------------------------------------------------------
// ¥ì¥¤¥ä¤ò¤½¤ì¤¾¤ì¤Îid¤ò¥­¡¼¤Ë¤·¤ÆÊÝÂ¸¤¹¤ë¡£
//----------------------------------------------------------
var _component_list = new Array();

function _addComponent(component)
{
    var id = component.id();
    _component_list[id] = component;
}


//----------------------------------------------------------
// ¥ì¥¤¥ä¤ÎÆ©ÌÀÅÙ¤òÊÑ¹¹¤·¤Æ¤¤¤¯²áÄø¤Ç¥¿¥¤¥Þ¤«¤é¸Æ¤Ð¤ì¤ë´Ø¿ô¡£
// °ú¿ô¤Ç»ØÄê¤µ¤ì¤¿¥ì¥¤¥ä¤ÎÆ©ÌÀÅÙÊÑ¹¹¥á¥½¥Ã¥É¤ò¥³¡¼¥ë¤¹¤ë¡£
//----------------------------------------------------------
function _triggerChangeOpacity(id)
{
    var component = _component_list[id];
    component.changeOpacity();
}


//----------------------------------------------------------
// °ú¿ô¤ÇÅÏ¤µ¤ì¤¿ÊÑ¿ô¤¬½é´ü²½ºÑ¤ß¤ÎÊÑ¿ô¤«¤òÈ½Äê¤¹¤ë¡£
//----------------------------------------------------------
function _defined(val)
{
    return (typeof(val) != "undefined") ? true : false;
}



/*==================================================================*/
// ¥Ü¡¼¥À¡¼¥¯¥é¥¹¤ÎÄêµÁ
// ¥Ç¥Õ¥©¥ë¥È¤ÎÀßÄê¤Ï "1px solid #000000"
/*==================================================================*/
function Border()
{
    this._width = 1;
    this._style = "solid";
    this._color = "#000000";
}


//----------------------------------------------------------
// ¥Ü¡¼¥À¡¼¤Î¿§¤ÎÀßÄê¡¢¼èÆÀ¤ò¹Ô¤¦¥á¥½¥Ã¥É¡£
//
// var boder = new Border();
// boder.color("#ffffcc");
// alert(boder.color());
//----------------------------------------------------------
function _Border_Color(color)
{
    if(!_defined(color)){
        return this._color;
    }else{
        this._color = color;
    }
}
Border.prototype.color = _Border_Color;


//----------------------------------------------------------
// ¥Ü¡¼¥À¡¼¤Î¥¹¥¿¥¤¥ë¤ÎÀßÄê¡¢¼èÆÀ¤ò¹Ô¤¦¥á¥½¥Ã¥É¡£
// ¥¹¥¿¥¤¥ë¤Ï°Ê²¼¤ÎÃÍ¤òÀßÄê¤¹¤ë¡£
//   none, hidden, solid, double, groove,
//   ridge, inset, outset, dashed, dotted
//
// var boder = new Border();
// boder.style("dashed");
// alert(boder.style());
//----------------------------------------------------------
function _Border_Style(style)
{
    if(!_defined(style)){
        return this._style;
    }else{
        this._style = style;
    }
}
Border.prototype.style = _Border_Style;


//----------------------------------------------------------
// ¥Ü¡¼¥À¡¼¤ÎÉý¤ÎÀßÄê¡¢¼èÆÀ¤ò¹Ô¤¦¥á¥½¥Ã¥É¡£
//
// var boder = new Border();
// boder.width(10);
// alert(boder.width());
//----------------------------------------------------------
function _Border_Width(width)
{
    if(!_defined(width)){
        return this._width;
    }else{
        this._width = width;
    }
}
Border.prototype.width = _Border_Width;




/*==================================================================*/
// ¥Ö¥é¥¦¥¶¾å¤Î¥Þ¥¦¥¹¤Î°ÌÃÖ¤òÊáÂª¤¹¤ë¤¿¤á¤Î´Ø¿ô·´
// ¥Þ¥¦¥¹¤Î°ÌÃÖ¾ðÊó¤ò¼èÆÀ¤¹¤ë¤Ë¤Ï°Ê²¼¤Î2¤Ä¤Î´Ø¿ô¤ò»ÈÍÑ¤¹¤ë¡£
//
//   getCurrentMouseX()
//   getCurrentMouseY()
//
/*==================================================================*/
document.onmousemove = _documentMouseMove;

var _mousePosX = 0;
var _mousePosY = 0;

function _documentMouseMove(evt)
{
    _mousePosX = _getEventX(evt);
    _mousePosY = _getEventY(evt);
}

function _getEventX(evt)
{
    var ret;
    if(Netscape){
        ret = evt.pageX;
    }else if(MSIE){
        ret = event.x + getPageXOffset();
    }else if(Safari){
        ret = event.x + getPageXOffset();
    }else{
        ret = evt.x;
    }

    return ret;
}

function _getEventY(evt)
{
    var ret;

    if(Netscape){
        ret = evt.pageY;
    }else if(MSIE){
        ret = event.y + getPageYOffset();
    }else if(Safari){
        ret = event.y + getPageYOffset();
    }else{
        ret = event.y;
    }

    return ret;
}

//----------------------------------------------------------
// ¥Ö¥é¥¦¥¶¾å¤Î¥Þ¥¦¥¹¥Ý¥¤¥ó¥¿¤ÎXºÂÉ¸¤òÊÖ¤¹¡£
//----------------------------------------------------------
function getCurrentMouseX()
{
    return _mousePosX;
}

//----------------------------------------------------------
// ¥Ö¥é¥¦¥¶¾å¤Î¥Þ¥¦¥¹¥Ý¥¤¥ó¥¿¤ÎYºÂÉ¸¤òÊÖ¤¹¡£
//----------------------------------------------------------
function getCurrentMouseY()
{
    return _mousePosY
}


//----------------------------------------------------------
// ¥¹¥¯¥í¡¼¥ë¤µ¤ì¤Æ¤¤¤ë»þ¤ÎÉ½¼¨³«»Ï°ÌÃÖ¡ÊX¼´¡Ë¤òÊÖ¤¹¡£
//----------------------------------------------------------
function getPageXOffset()
{
    var ret;
    if(Safari || Opera){
        ret = document.body.scrollLeft;
    }else{
        if(document.body.scrollLeft > 0){
            ret = document.body.scrollLeft;
        }else{
            ret = document.documentElement.scrollLeft;
        }
    }

    return ret;
}


//----------------------------------------------------------
// ¥¹¥¯¥í¡¼¥ë¤µ¤ì¤Æ¤¤¤ë»þ¤ÎÉ½¼¨³«»Ï°ÌÃÖ¡ÊY¼´¡Ë¤òÊÖ¤¹¡£
//----------------------------------------------------------
function getPageYOffset()
{
    var ret;
    if(Safari || Opera){
        ret = document.body.scrollTop;
    }else{
        if(document.body.scrollTop > 0){
            ret = document.body.scrollTop;
        }else{
            ret = document.documentElement.scrollTop;
        }
    }

    return ret;
}
