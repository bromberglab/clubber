<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'IndexController@home')->name('home');

Auth::routes();

Route::get('/project/datatables/{id?}', 'ProjectController@datatables')->name('project.datatables');

Route::get('/project/model/{id?}', 'ProjectController@model')->name('project.model');
Route::post('/project/processing', 'ProjectController@store_processing')->name('project.store_processing');
Route::post('/project/permissions', 'ProjectController@change_permissions')->name('project.change_permissions');
Route::resource('project', 'ProjectController');

Route::get('/project_job/datatables/{id?}', 'Project_JobController@datatables')->name('project_job.datatables');
Route::get('/project_job/model/{id?}', 'Project_JobController@model')->name('project_job.model');
Route::get('/project_job/download/{id?}', 'Project_JobController@download')->name('project_job.download');
Route::resource('project_job', 'Project_JobController');

Route::get('/job/datatables/{id?}', 'JobController@datatables')->name('job.datatables');
Route::get('/job/model/{id?}', 'JobController@model')->name('job.model');
Route::get('/job/download/{id?}', 'JobController@download')->name('job.download');
Route::get('/job/csv/{id?}', 'JobController@csv')->name('job.csv');
Route::resource('job', 'JobController');

Route::get('/config', 'IndexController@config_read')->middleware(['auth', 'isAdmin'])->name('config');
Route::post('/config', 'IndexController@config_write_ajax')->middleware(['auth', 'isAdmin'])->name('config_process_ajax');

Route::get('/cluster/refresh/{id?}', 'ClusterController@refresh')->name('cluster.refresh');
Route::resource('cluster', 'ClusterController');

Route::get('logs', 'LogController@index')->name('logs');

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');

Auth::routes();

Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function () {
  Route::get('/project/datatables/{id?}', 'ProjectController@datatables');
  Route::get('/job/datatables/{id?}', 'JobController@datatables');
});
