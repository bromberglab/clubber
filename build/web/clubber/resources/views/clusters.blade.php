@extends('template.layout')

@push('scripts')
<script>
  $(function(){ // begin on DOM ready
    $('.cluster-refresh').on('click', function (e) {
        $.ajaxSetup({
            header:$('meta[name="_token"]').attr('content')
        })
        console.log('POST request: ' + this.id);
        var trigger = $( this );
        var cluster_id = trigger.data('cluster');
        e.preventDefault();
        
        // if(trigger.data('action')=="refresh") {
        //   waitingDialog.show('Updating cluster ' + cluster_id + '...', {dialogSize: 'sm'});
        // }

        var $cluster = $('#clusters-' + cluster_id);

        $cluster.find('.cluster_stats_wrapper').hide();
        $cluster.find('.loader').show();

        $.ajax({
          type: 'GET',
          url: '{{ route('cluster.refresh') }}/' + cluster_id,
          data: null,
          cache       : false,
          contentType : false,
          processData : false
        })
        .done(function(data) {
            console.log('POST success: ' + trigger.attr('id'));
            console.log(data);
            $cluster.find('.updated').text(data['updated']);
            $cluster.find('.connection').text(data['connection']);
            $cluster.find('.workload').text(data['workload']);
            $cluster.find('.storage').text(data['storage']);
            $cluster.find('.shell').text(data['shell']);
            $cluster.find('.connection').text(data['connection']);
            if(data['connection_msg'])
            {
              $cluster.find('.connection_msg').text(data['connection_msg']);
              $cluster.find('.connection_msg').show();
            } else {
              $cluster.find('.connection_msg').hide();
            }

            if(data['msg'])
            {
              BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DEFAULT,
                title: 'Success',
                message: data['msg'],
              });
            }
        })
        .fail(function(data){
            console.log('POST error: ' + trigger.attr('id'));
            var errors = data.responseJSON;
            var $msg = $('<ul/>');
            if (errors.length == 0)
                $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
            _.forEach(errors, function(value, key) {
                $msg.append( $("<li>"+value+"</li>") );
            } )
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
                title: 'Error',
                message: $msg,
            });
        })
        .always(function(data) {
            // if(trigger.data('action')=="refresh") {
            //   waitingDialog.hide();
            // }
            $cluster.find('.cluster_stats_wrapper').show();
            $cluster.find('.loader').hide();
        });
    });

    $('.cluster-refresh + .hidden').click();
  }); // end on DOM ready
</script>
@endpush

@section('content')
<div class="panel panel-default">
  <div class="panel-body"
      style="text-align: center; width: 45%; margin:
      auto;font-size:1.2em;position:relative">
      @foreach ($clusters as $cluster)
    <div id="home-content">
      <div id='clusters-{{ $cluster['title'] }}'>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{ $cluster['title'] }}</h3>
          </div>
          <div class="panel-body cluster_stats_wrapper" style="text-align: left; display: none;">
            <h4>Connection: <span class="label label-default connection cluster_stats">{{ $cluster['connection'] }}</span></h4>
            @if ($cluster['connection_msg'] != NULL)
              <div class="alert alert-danger connection_msg cluster_stats"><em> {!! $cluster['connection_msg'] !!}</em></div>
            @endif
            <h4>Shell: <span class="label label-default shell cluster_stats">{{ $cluster['shell'] }}</span></h4>
            <h4>Storage (used): <span class="label label-default storage cluster_stats">{{ $cluster['storage'] }}</span></h4>
            <h4>Workload: <span class="label label-default workload cluster_stats">{{ $cluster['workload'] }}</span></h4>
            <h4>Updated: <span class="label label-default updated cluster_stats">{{ $cluster['updated'] }}</span></h4>
          </div>
          <div class="loader">Loading...</div>
          <div class="panel-footer">
            <button type="button" data-cluster="{{ $cluster['title'] }}" data-action="refresh" class="btn btn-default cluster-refresh">Refresh</button>
            <button type="button" data-cluster="{{ $cluster['title'] }}" data-action="load" class="btn btn-default cluster-refresh hidden"></button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@stop
