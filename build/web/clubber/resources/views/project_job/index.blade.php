@extends('template.layout')

@push('scripts')
<script>
$(function() { // begin on DOM ready
  var $table = $('#datatable').DataTable({
      processing: true,
      deferRender: true,
      serverSide: false,
      ajax: '{!! route('project_job.datatables') !!}',
      rowId: 'id',
      select: false,
      statesave: false,
      dom: '<"H"Blf>rt<"F"ip><"clear">',
      lengthMenu: [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
      iDisplayLength: 10,
      order: [[ 0, 'desc' ]],
      // select: {
      //     'style': 'multi'
      // },
      buttons: [
          {
              extend: 'csvHtml5',
              exportOptions: {
                  columns: [1,2,3,4]
              }
          }
      ],
      columns: [
          { data: 'id', name: 'id' },
          { data: 'project', name: 'project' },
          { data: 'status', name: 'status' },
          { data: 'progress', name: 'progress' },
          { data: null, name: 'controls' }
      ],
      // responsive: {
      //       details: {
      //           type: 'column',
      //           target: -1
      //       }
      // },
      columnDefs: [
          {
              'targets': -1,
              'data': null,
              'width': '85px',
              'orderable': false,
              'searchable': false,
              //'defaultContent':
              'render': function ( data, type, row ) {
                  var content = '<a class="row-download" href="{{ route('project_job.download') }}" data-action="check" data-project="' + row.project + '" data-target="' + row.id + '" aria-label="Download"><i class="fa fa-download" aria-hidden="true"></i></a>';
                  content += '<a class="row-control-disabled" aria-label="Settings"><i class="fa fa-cog" aria-hidden="true"></i></a>';
                  content += '<a class="row-control project-job-destroy" href="{{ route('project_job.destroy', ['id' => NULL]) }}/' + row.id + '" aria-label="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                  content += '<a class="row-control project-job-details" href="{{ route('project_job.model') }}/' + row.id + '" aria-label="Details"><i class="fa fa-bars" aria-hidden="true"></i></a>';
                  return content;
              }
          },
          {
              'targets': 0,
              'title': '#',
              'visible': true,
              'searchable': true,
              'width': '50px',
              'render': function ( data, type, row ) {
                  return data;
              }
          },
          {
              'targets': 1,
              'title': 'Project',
              'render': function ( data, type, row ) {
                  var projectid2name = {!! json_encode($projectid2name) !!};
                  return projectid2name[data];
              }
          },
          {
              'targets': 2,
              'title': 'Status',
              'render': function ( data, type, row ) {
                  var status_text = {
                    0: 'unassigned',
                    1: 'queued',
                    2: 'running',
                    3: 'finished',
                    4: 'failed'
                  };
                  return status_text[data];
              }
          },
          {
              'targets': 3,
              'title': 'Progress',
              'render': function ( data, type, row ) {
                  return data + '%';
              }
          }
      ],
      drawCallback: function(settings) {

          // project destroy
          $('.project-job-destroy').on('click', function (e) {
              e.preventDefault();
              waitingDialog.show('Removing submissions from remote clusters...', {dialogSize: 'sm'});
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'DELETE',
                  data: {
                      '_token': '{{ csrf_token() }}'
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  $('#datatable').DataTable().ajax.reload();
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              })
              .always(function(data) {
                    waitingDialog.hide();
              });
          });

          // project_job details
          $('.project-job-details').on('click', function (e) {
              e.preventDefault();
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'GET',
                  data: {
                      '_token': '{{ csrf_token() }}'
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    title: 'Submission details',
                    message: data,
                  });
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              });
          });

          // project_job downlaods
          $('.row-download').on('click', function (e) {
              e.preventDefault();
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'GET',
                  data: {
                      '_token': '{{ csrf_token() }}',
                      'project': trigger.data('project'),
                      'target': trigger.data('target'),
                      'action': trigger.data('action')
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    title: 'Download',
                    message: data,
                  });
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              });
          });
      },
      initComplete: function(settings, json) {
          console.log('datatable created');
          var table =  $('#datatable').DataTable();
          var oTable =  $('#datatable').dataTable();
      }
  });

  $('#template-modal').on('show.bs.modal', function (e) {
      $('.modal .project-job-selectpicker').selectpicker()
      .on('changed.bs.select', function (e) {
        $('.project-details').addClass('hidden');
        $('.modal .project-' + e.target.value + '-details').removeClass('hidden');
      });
      $('.modal .project-'
        + $('.modal .project-job-selectpicker').selectpicker('val')
        + '-details').removeClass('hidden');
      $('.modal').on('change', ':file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
              console.log(label);
          input.trigger('fileselect', [numFiles, label]);
      });
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
      });
  });

  $('#template-modal-submit').on('click', function (e) {
      // $(".modal #form-project-job-new").submit();
      var form = $(".modal #form-project-job-new");
      $.ajaxSetup({
          header:$('meta[name="_token"]').attr('content')
      })
      console.log('POST request: ' + this.id);
      var trigger = $( this );
      e.preventDefault();
      waitingDialog.show('Processing submission...', {dialogSize: 'sm'});

      var formData = false;
      if (window.FormData){
          formData = new FormData(form[0]);
      } else {
        console.log('AJAX file upload impossible, switching to regular submit.');
        form.submit();
        return;
      }

      $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: formData ? formData : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false
      })
      .done(function(data) {
          console.log('POST success: ' + trigger.attr('id'));
          console.log(data);
          $('#datatable').DataTable().ajax.reload();
          $('#template-modal').modal('hide');
      })
      .fail(function(data){
          console.log('POST error: ' + trigger.attr('id'));
          var errors = data.responseJSON;
          var $msg = $('<ul/>');
          if (errors.length == 0)
              $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
          _.forEach(errors, function(value, key) {
              $msg.append( $("<li>"+value+"</li>") );
          } )
          BootstrapDialog.show({
              type: BootstrapDialog.TYPE_DANGER,
              title: 'Error',
              message: $msg,
          });
      })
      .always(function(data) {
            waitingDialog.hide();
      });
  });

}); // end on DOM ready
</script>
@endpush

@section('content')
  <div>
    <table class="table table-hover table-condensed table-bordered table-striped" id="datatable"></table>
  </div>
  <div class="clubber-controls">
    <div class="">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#template-modal" data-modaltitle="New Submission" data-modalcontent="#new-submission" data-backdrop="false"><i class="fa fa-plus" aria-hidden="true"></i> New Submission</button>
    </div>
    <div id="new-submission" class="hide">
      {!! Form::open(['action' => 'Project_JobController@store', 'files' => true, 'id' => 'form-project-job-new']) !!}
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Submission</h3>
          </div>
          <div class="panel-body">
            <div class="input-group project-job-selection">
              <span class="input-group-addon new-project-job-input">Project</span>
              {{ Form::select('size', $projectid2name, 'default', ['name' => 'project', 'class' => 'project-job-selectpicker']) }}
            </div>
            <div class="input-group">
              <!-- <span class="input-group-addon new-project-job-input">Input file</span> -->
              <label class="input-group-btn">
                <span class="btn btn-info btn-file">
                  Input file... {{ Form::file('project_job_file', [
                  'id' => 'project-job-file',
                  'class' => 'form-control',
                  'placeholder' => 'Submission job file...',
                  'aria-describedby' => 'basic-addon2'
                  ]) }}
                </span>
              </label>
              <input type="text" class="form-control" placeholder="(optional)..." readonly>
            </div>
          </div>
          <div class="panel-footer">
            @foreach ($project_job_details as $project_id => $details)
              <div class="project-{{ $project_id }}-details project-details hidden">
                <p>Pre-processer: {{ $details[0] or 'None' }}</p>
                <p>Pre-processer: {{ $details[1] or 'None' }}</p>
              </div>
            @endforeach
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Arguments</h3>
          </div>
          <div class="panel-body">
            <div class="input-group">
              <span class="input-group-addon new-project-job-input">Input arguments</span>
              {{ Form::textarea('project_job_arguments', null, [
                'id' => 'project-job-arguments',
                'class' => 'form-control',
                'placeholder' => 'Submission arguments: 1 job per line...',
                'aria-describedby' => 'basic-addon2'
                ]) }}
            </div>
            <span class="input-group project-job-selection"><b>or</b></span>
            <div class="input-group">
              <!-- <span class="input-group-addon new-project-job-input">Input file</span> -->
              <label class="input-group-btn">
                <span class="btn btn-info btn-file">
                  Argument file... {{ Form::file('project_job_argument_file', [
                  'id' => 'project-job-argument-file',
                  'class' => 'form-control',
                  'placeholder' => 'Submission argument file...',
                  'aria-describedby' => 'basic-addon2'
                  ]) }}
                </span>
              </label>
              <input type="text" class="form-control" placeholder="(optional)..." readonly>
            </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
@stop
