@extends('template.layout')

@section('title')
@stop

@section('navbar')
@stop

@section('content')
<div class="flex-center position-ref height-95">
    <div class="content">
        <div class="title m-b-md">
            <i>{{ config('app.name') }}</i>
        </div>

        <div class="links">
            <a href="{{ route('home') }}">BrombergLab Services</a>
        </div>
    </div>
</div>
@stop
