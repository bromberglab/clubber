@extends('template.layout')

@push('scripts')
<script>
$(function() { // begin on DOM ready
  var $table = $('#datatable').DataTable({
      processing: true,
      deferRender: true,
      serverSide: false,
      ajax: '{!! route('project.datatables') !!}',
      rowId: 'id',
      select: false,
      statesave: false,
      dom: '<"H"Blf>rt<"F"ip><"clear">',
      lengthMenu: [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
      iDisplayLength: 10,
      order: [[ 0, 'desc' ]],
      // select: {
      //     'style': 'multi'
      // },
      buttons: [
          {
              extend: 'csvHtml5',
              exportOptions: {
                  columns: [1,2,3]
              }
          }
      ],
      columns: [
          { data: 'id', name: 'id' },
          { data: 'name', name: 'name' },
          { data: 'description', name: 'description' },
          { data: null, name: 'controls' }
      ],
      // responsive: {
      //       details: {
      //           type: 'column',
      //           target: -1
      //       }
      // },
      columnDefs: [
          {
              'targets': -1,
              'data': null,
              'width': '50px',
              'orderable': false,
              //'defaultContent': `
              'render': function ( data, type, row ) {
                  //aria-label="Settings" data-toggle="modal" data-target="#template-modal" data-modaltitle="@mtitle" data-modalcontent="@mcontent"
                  var content = '<div class="row-control-dropdown dropdown">';
                  content += '<a class="row-control" aria-label="Settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i></a>';
                  content += `<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li class="dropdown-header">Settings</li>`;
                  content += '<li><a class="project-settings-permissions" data-toggle="modal" data-target="#template-modal" data-modaltitle="Project Access" data-modalcontent="#project-permissions" data-backdrop="false" data-projectid="' + row['id'] + '">Permissions</a></li>';
                      // <li role="separator" class="divider"></li>
                  content += `
                          </ul>
                        </div>`;
                  content += '<a class="row-control project-destroy" href="{{ route('project.destroy', ['id' => NULL]) }}/' + row['id'] + '" aria-label="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                  content += '<a class="row-control project-details" href="{{ route('project.model') }}/' + row['id'] + '" aria-label="Details"><i class="fa fa-bars" aria-hidden="true"></i></a>';
                  return content;
              }
          },
          {
              'targets': 0,
              'title': '#',
              'visible': true,
              'searchable': true,
              'width': '50px',
              'render': function ( data, type, row ) {
                  return data;
              }
          },
          {
              'targets': 1,
              'title': 'Name',
          },
          {
              'targets': 2,
              'title': 'Description',
          }
      ],
      drawCallback: function(settings) {

          // project destroy
          $('.project-destroy').on('click', function (e) {
              e.preventDefault();
              waitingDialog.show('Removing project from remote clusters...', {dialogSize: 'sm'});
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'DELETE',
                  data: {
                      '_token': '{{ csrf_token() }}'
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  $('#datatable').DataTable().ajax.reload();
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              })
              .always(function(data) {
                    waitingDialog.hide();
              });
          });

          // project details
          $('.project-details').on('click', function (e) {
              e.preventDefault();
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'GET',
                  data: {
                      '_token': '{{ csrf_token() }}'
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    title: 'Project details',
                    message: data,
                  });
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              });
          });
      },
      initComplete: function(settings, json) {
          console.log('datatable created');
          var table =  $('#datatable').DataTable();
          var oTable =  $('#datatable').dataTable();
      }
  });

  $('#template-modal').on('show.bs.modal', function (e) {
      $($('.modal #cluster-selectpicker option')[0])
      .prop('disabled',true)
      .removeAttr('selected')
      .addClass('hidden');
      $('.modal .project-selectpicker').selectpicker();
      $('.modal .project-selectpicker-cluster').selectpicker()
      .on('changed.bs.select', function (e) {
        $('.cluster-parameters-input').addClass('hidden');
        $('.modal .project-selectpicker-cluster').find("option:selected").each(function() {
          $('.modal .cluster-' + $( this ).text() + '-parameters-input').removeClass('hidden');
        });
      });
      $('.modal .advanced-toggle').click(function() {
          $('.modal .input-hidden').slideToggle('slow');
      });
      $('.modal #project-transfer-switch').attr('id', 'project-transfer-switch-modal');
      $('.modal #project-transfer-switch-label').attr('for', 'project-transfer-switch-modal');
      $('.modal #project-dbstore-switch').attr('id', 'project-dbstore-switch-modal');
      $('.modal #project-dbstore-switch-label').attr('for', 'project-dbstore-switch-modal');
      $('.modal').on('change', ':file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
              console.log(label);
          input.trigger('fileselect', [numFiles, label]);
      });
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
      });
      $('.modal #project-name')
      .on('keypress', function(e) {
          if (e.which == 32)
              return false;
      })
      .on('keyup', function(e) {
          if ((this.value).includes(" "))
            this.value = this.value.replace(" ","_");
      });
      $('.modal .project-user-permissions').each(function( index ) {
          var project_id = $(e.relatedTarget).data('projectid');
          var owner = $.inArray(project_id, $(this).data('user-projects'));
          if (owner != -1) {
              $(this).prop( "checked", true );
              $(this).prop( "disabled", true );
              $(this).parent().append( " (owner)" );
          }
          var access = $.inArray(project_id, $(this).data('user-access'));
          if (access != -1) {
              $(this).prop( "checked", true );
          }
          $('.modal .project-id-permission').each(function( index ) {
              $(this).val( project_id );
          });
      });
  });

  $('#template-modal-submit').on('click', function (e) {
      // $(".modal #form-project-new").submit();
      var form = $('#template-modal form');
      $.ajaxSetup({
          header:$('meta[name="_token"]').attr('content')
      })
      console.log('POST request: ' + this.id);
      var trigger = $( this );
      $reload = form.data('reload');
      if ($reload) {
          form.submit();
          return;
      } else {
          e.preventDefault();
      }

      waitingDialog.show('Creating project and copying it to clusters...', {dialogSize: 'sm'});

      var formData = false;
      if (window.FormData){
          formData = new FormData(form[0]);
      } else {
          console.log('AJAX file upload impossible, switching to regular submit.');
          form.submit();
          return;
      }

      $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: formData ? formData : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false
      })
      .done(function(data) {
          console.log('POST success: ' + trigger.attr('id'));
          console.log(data);
          $('#datatable').DataTable().ajax.reload();
          $('#template-modal').modal('hide');
          if(data['msg'])
          {
            BootstrapDialog.show({
              type: BootstrapDialog.TYPE_DEFAULT,
              title: 'Success',
              message: data['msg'],
            });
          }
      })
      .fail(function(data){
          console.log('POST error: ' + trigger.attr('id'));
          var errors = data.responseJSON;
          var $msg = $('<ul/>');
          if (errors.length == 0)
              $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
          _.forEach(errors, function(value, key) {
              $msg.append( $("<li>"+value+"</li>") );
          } )
          BootstrapDialog.show({
              type: BootstrapDialog.TYPE_DANGER,
              title: 'Error',
              message: $msg,
          });
      })
      .always(function(data) {
            waitingDialog.hide();
      });
  });

}); // end on DOM ready
</script>
@endpush

@section('content')
  <div>
    <table class="table table-hover table-condensed table-bordered table-striped" id="datatable"></table>
  </div>
  <div class="clubber-controls">
    <div class="clubber-controls-btn">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#template-modal" data-modaltitle="New project" data-modalcontent="#new-project" data-backdrop="false"><i class="fa fa-plus" aria-hidden="true"></i> New project</button>
    </div>
    <div class="clubber-controls-btn">
      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#template-modal" data-modaltitle="Add Pre-processor" data-modalcontent="#new-preprocessor" data-backdrop="false"><i class="fa fa-plus" aria-hidden="true"></i> New Pre-processor</button>
    </div>
    <div class="clubber-controls-btn">
      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#template-modal" data-modaltitle="Add Post-processor" data-modalcontent="#new-postprocessor" data-backdrop="false"><i class="fa fa-plus" aria-hidden="true"></i> New Post-processor</button>
    </div>
    <div id="new-preprocessor" class="hide">
      {!! Form::open(['action' => 'ProjectController@store_processing', 'files' => true, 'id' => 'form-preprocessing-new']) !!}
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Pre-processor</h3>
          </div>
          <div class="panel-body">
            <div class="input-group">
              <!-- <span class="input-group-addon new-project-input">Script</span> -->
              <label class="input-group-btn">
                <span class="btn btn-info btn-file">
                  Script... {{ Form::file('preprocessing_script', [
                  'id' => 'preprocessing-script',
                  'class' => 'form-control',
                  'placeholder' => 'Pre-processing script file...',
                  'aria-describedby' => 'basic-addon2',
                  'display' => 'none',
                  ]) }}
                </span>
              </label>
              <input type="text" class="form-control" readonly>
            </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
    <div id="new-postprocessor" class="hide">
      {!! Form::open(['action' => 'ProjectController@store_processing', 'files' => true, 'id' => 'form-postprocessing-new']) !!}
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Post-processor</h3>
          </div>
          <div class="panel-body">
            <div class="input-group">
              <!-- <span class="input-group-addon new-project-input">Script</span> -->
              <label class="input-group-btn">
                <span class="btn btn-info btn-file">
                  Script... {{ Form::file('postprocessing_script', [
                  'id' => 'postprocessing-script',
                  'class' => 'form-control',
                  'placeholder' => 'Post-processing script file...',
                  'aria-describedby' => 'basic-addon2',
                  'display' => 'none',
                  ]) }}
                </span>
              </label>
              <input type="text" class="form-control" readonly>
            </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
    <div id="new-project" class="hide">
      {!! Form::open(['action' => 'ProjectController@store', 'files' => true, 'id' => 'form-project-new']) !!}
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Project</h3>
          </div>
          <div class="panel-body">
            <div class="input-group">
              <span class="input-group-addon new-project-input">Name</span>
              {{ Form::input('text', 'project_name', null, [
                'id' => 'project-name',
                'class' => 'form-control',
                'placeholder' => 'Project name...',
                'aria-describedby' => 'basic-addon2'
                ]) }}
            </div>
            <div class="input-group">
              <span class="input-group-addon new-project-input">Description</span>
              {{ Form::input('text', 'project_description', null, [
                'id' => 'project-description',
                'class' => 'form-control',
                'placeholder' => 'Project description...',
                'aria-describedby' => 'basic-addon2'
                ]) }}
            </div>
            <div class="input-group">
              <!-- <span class="input-group-addon new-project-input">Script</span> -->
              <label class="input-group-btn">
                <span class="btn btn-info btn-file">
                  Script... {{ Form::file('project_script', [
                  'id' => 'project-script',
                  'class' => 'form-control',
                  'placeholder' => 'Project script file...',
                  'aria-describedby' => 'basic-addon2',
                  'display' => 'none',
                  ]) }}
                </span>
              </label>
              <input type="text" class="form-control" placeholder="Main project script file..." readonly>
            </div>
            <div class="input-group project-selection">
              <span class="input-group-addon new-project-input">Clusters</span>
              {{ Form::select('project_cluster[]', $enabled_clusters, null, ['multiple' => true, 'name' => 'project_cluster[]', 'class' => 'project-selectpicker-cluster', 'title' => 'Nothing selected' ,'id' => 'cluster-selectpicker']) }}
            </div>
            <div class="cluster-parameters">
              @foreach ($enabled_clusters as $cluster_id => $cluster_name)
                @if ($cluster_id)
                <div class="cluster-{{ $cluster_id }}-parameters-input cluster-parameters-input hidden">
                  <div class="input-group">
                    <span class="input-group-addon new-project-input">Parameters:<br/><br/>{{ $cluster_name }}</span>
                    {{ Form::textarea('cluster_'.$cluster_id.'_parameters', null, [
                      'id' => 'cluster-'.$cluster_id.'-parameters',
                      'class' => 'form-control',
                      'placeholder' => 'Cluster '.$cluster_name.' parameters...',
                      'aria-describedby' => 'basic-addon2'
                      ]) }}
                  </div>
                </div>
                @endif
              @endforeach
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Options</h3>
          </div>
          <div class="panel-body">
            <div class="input-group">
              <!-- <span class="input-group-addon new-project-input">Sources</span> -->
              <label class="input-group-btn">
                <span class="btn btn-info btn-file">
                  Sources... {{ Form::file('project_sources', [
                  'id' => 'project-sources',
                  'class' => 'form-control',
                  'placeholder' => 'Project source file...',
                  'aria-describedby' => 'basic-addon2'
                  ]) }}
                </span>
              </label>
              <input type="text" class="form-control" placeholder="ZIP file containing additional sources (optional)..." readonly>
            </div>
            <div class="input-group project-selection">
              <span class="input-group-addon new-project-input">Pre-processor</span>
              {{ Form::select('pre_processor', $pre_processors, null, ['name' => 'pre_processor', 'class' => 'project-selectpicker']) }}
            </div>
            <div class="input-group project-selection">
              <span class="input-group-addon new-project-input">Post-processor</span>
              {{ Form::select('post_processor', $post_processors, null, ['name' => 'post_processor', 'class' => 'project-selectpicker']) }}
            </div>
            <div class="input-group project-selection-checkbox">
              <ul class="list-group">
                <li class="list-group-item">
                    Retrieve results from remote clusters
                    <div class="material-switch pull-right">
                        <!-- <input id="someSwitchOptionDefault" name="project_transfer_switch" type="checkbox"/> -->
                        {{ Form::checkbox('project_transfer_switch', 1, true, [
                          'id' => 'project-transfer-switch',
                          'class' => 'form-control',
                          'aria-describedby' => 'basic-addon2'
                          ]) }}
                        <label id="project-transfer-switch-label" for="project-transfer-switch" class="label-default"></label>
                    </div>
                </li>
                <li class="list-group-item">
                    Additionally store job results in database
                    <div class="material-switch pull-right">
                        <!-- <input id="someSwitchOptionDefault" name="project_transfer_switch" type="checkbox"/> -->
                        {{ Form::checkbox('project_dbstore_switch', 1, false, [
                          'id' => 'project-dbstore-switch',
                          'class' => 'form-control',
                          'aria-describedby' => 'basic-addon2'
                          ]) }}
                        <label id="project-dbstore-switch-label" for="project-dbstore-switch" class="label-default"></label>
                    </div>
                </li>
              </ul>
            </div>
            <div class="input-group">
              <button class="btn btn-default btn-xs advanced-toggle" type="button">
                Advanced <span class="caret"></span>
              </button>
            </div>
            <div class="input-hidden" style="display:none">
              <div class="input-group">
                <span class="input-group-addon new-project-input">Transfer directory</span>
                {{ Form::input('text', 'project_transferdir', null, [
                  'id' => 'project-transferdir',
                  'class' => 'form-control',
                  'placeholder' => 'name of sub-directory to retrieve from cluster',
                  'aria-describedby' => 'basic-addon2'
                  ]) }}
              </div>
            </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
    <div id="project-permissions" class="hide">
      {!! Form::open(['action' => 'ProjectController@change_permissions', 'id' => 'form-project-permissions', 'data-reload' => true]) !!}
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Users</h3>
          </div>
          <div class="panel-body">
            <div class="input-group">
              {!! Form::hidden('project_id', null, array('class' => 'project-id-permission')) !!}
              @foreach (App\User::where('id', '!=', Auth::id())->get() as $user)
                @php ($user_access = [])
                @foreach ($user->permissions->pluck('name') as $permission)
                  @php
                    array_push($user_access, intval(explode(" ", $permission)[2]))
                  @endphp
                @endforeach
                <p>{!! Form::checkbox('project_permissions[]', $user->id, $user->hasRole('Admin'), array('class' => 'project-user-permissions', ($user->hasRole('Admin') ? 'disabled' : ''), 'data-user-access' => json_encode($user_access), 'data-user-projects' => App\Project::where('user', '=', $user->id)->pluck('id'))) !!} {{ $user->hasRole('Admin') ? $user->name . " (admin)" : $user->name }}</p>
              @endforeach
            </div>
          </div>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
@stop
