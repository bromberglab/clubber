@extends('template.layout')

@push('scripts')
<script>
$(function() { // begin on DOM ready
  var projectid2name = {!! json_encode($projectid2name) !!};

  var $table = $('#datatable').DataTable({
      processing: true,
      deferRender: true,
      serverSide: false,
      ajax: '{!! route('job.datatables') !!}',
      rowId: 'id',
      select: false,
      statesave: false,
      dom: '<"H"Blf>rt<"F"ip><"clear">',
      lengthMenu: [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
      iDisplayLength: 10,
      order: [[ 0, 'desc' ]],
      // select: {
      //     'style': 'multi'
      // },
      buttons: [
          {
              extend: 'csvHtml5',
              exportOptions: {
                  columns: [1,2,3,4]
              }
          }
      ],
      columns: [
          { data: 'id', name: 'id' },
          { data: 'project', name: 'project' },
          { data: 'project_job', name: 'project_job' },
          { data: 'cluster', name: 'cluster' },
          { data: 'jobid', name: 'jobid' },
          { data: 'arrayidx', name: 'arrayidx' },
          { data: 'status', name: 'status' },
          { data: 'start', name: 'start' },
          { data: 'end', name: 'end' },
          { data: 'restarts', name: 'restarts' },
          { data: null, name: 'controls' }
      ],
      // responsive: {
      //       details: {
      //           type: 'column',
      //           target: -1
      //       }
      // },
      columnDefs: [
          {
              'targets': -1,
              'data': null,
              'width': '85px',
              'orderable': false,
              'searchable': false,
              //'defaultContent':
              'render': function ( data, type, row ) {
                var project_name = projectid2name[row.project];
                var jobdir = row.jobid + (row.arrayidx ? '.' + row.arrayidx : '');
                var content = '<a class="row-control row-download" href="{{ route('job.download') }}" data-action="check" data-project="' + row.project + '" data-project-job="' + row.project_job + '" data-target="' + jobdir + '" aria-label="Download"><i class="fa fa-download" aria-hidden="true"></i></a>';
                  content += '<a class="row-control-disabled csv-download" href="#" data-toggle="modal" data-target="#template-modal" data-modaltitle="Job Results" data-modalcontent="#csv-table" data-backdrop="true" data-jobid="' + row.id + '" data-project="' + row.project + '" data-project-job="' + row.project_job + '" data-target="' + jobdir + '" aria-label="CSV"><i class="fa fa-table" aria-hidden="true"></i></a>';
                  content += '<div class="row-control-dropdown dropdown">';
                  content += '<a class="row-control" aria-label="Settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-cog" aria-hidden="true"></i></a>';
                  content += `<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li class="dropdown-header">Settings</li>`;
                  content += '<li><a class="job-restart" href="{{ route('job.update', ['job' => NULL]) }}/' + row['id'] + '">Restart</a></li>';
                      // <li role="separator" class="divider"></li>
                  content += `
                          </ul>
                        </div>
                      <a class="job-delete row-control-disabled" aria-label="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></a>`;
                  content += '<a class="row-control job-details" href="{{ route('job.model') }}/' + row['id'] + '" aria-label="Details"><i class="fa fa-bars" aria-hidden="true"></i></a>';
                  return content;
              }
          },
          {
              'targets': 0,
              'title': '#',
              'visible': true,
              'searchable': true,
              'width': '50px',
              'render': function ( data, type, row ) {
                  return data;
              }
          },
          {
              'targets': 1,
              'title': 'Project',
              'render': function ( data, type, row ) {
                  return projectid2name[data];
              }
          },
          {
              'targets': 2,
              'title': 'Submission',
          },
          {
              'targets': 3,
              'title': 'Cluster',
          },
          {
              'targets': 4,
              'title': 'Job ID',
          },
          {
              'targets': 5,
              'title': 'Array ID',
          },
          {
              'targets': 6,
              'title': 'Status',
              'render': function ( data, type, row ) {
                  var status_text = {
                    0: 'unassigned',
                    1: 'queued',
                    2: 'running',
                    3: 'finished',
                    4: 'failed'
                  };
                  return status_text[data];
              }
          },
          {
              'targets': 7,
              'title': 'Start',
          },
          {
              'targets': 8,
              'title': 'End',
          },
          {
              'targets': 9,
              'title': '<i class="fa fa-refresh" aria-hidden="true"></i>',
          },
      ],
      drawCallback: function(settings) {

          // job restart
          $('.job-restart').on('click', function (e) {
              e.preventDefault();
              waitingDialog.show('Restarting job...', {dialogSize: 'sm'});

              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'PUT',
                  data: {
                      '_token': '{{ csrf_token() }}',
                      'action': 'restart'
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  $('#datatable').DataTable().ajax.reload();
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              })
              .always(function(data) {
                    waitingDialog.hide();
              });
          });

          // job details
          $('.job-details').on('click', function (e) {
              e.preventDefault();
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'GET',
                  data: {
                      '_token': '{{ csrf_token() }}'
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    title: 'Job details',
                    message: data,
                  });
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              });
          });

          // job downloads
          $('.row-download').on('click', function (e) {
              e.preventDefault();
              var trigger = $( this ),
                  url = trigger.attr("href");
              // $.ajaxSetup({
              //     header:$('meta[name="_token"]').attr('content')
              // });
              $.ajax({
                  url: url,
                  type: 'GET',
                  data: {
                      '_token': '{{ csrf_token() }}',
                      'project': trigger.data('project'),
                      'project_job': trigger.data('project-job'),
                      'target': trigger.data('target'),
                      'action': trigger.data('action')
                  }
              })
              .done(function(data) {
                  console.log('POST success: ' + trigger.attr('id'));
                  console.log(data);
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DEFAULT,
                    title: 'Download',
                    message: data,
                  });
              })
              .fail(function(data) {
                  console.log('POST error: ' + trigger.attr('id'));
                  var errors = data.responseJSON;
                  var $msg = $('<ul/>');
                  if (errors.length == 0)
                     $msg.append( $('<li>An error occured, please <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
                  _.forEach(errors, function(value, key) {
                     $msg.append( $("<li>"+value+"</li>") );
                  } )
                  BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Error',
                    message: $msg,
                  });
              });
          });

          $('.csv-download').each(function (e) {
              var trigger = $( this );
              $.ajax({
                  url: '{{ route('job.csv') }}' + '/' + trigger.data('jobid'),
                  type: 'GET',
                  data: {
                      '_token': '{{ csrf_token() }}'
                  },
                  error: function(data)
                  {
                      console.log(data);
                  },
                  success: function(data)
                  {
                      if ($.trim(data)) {
                          trigger.data("csv-file", data);
                          trigger.toggleClass("row-control");
                          trigger.toggleClass("row-control-disabled");
                      } else {
                          trigger.removeAttr("href");
                          trigger.removeAttr("data-toggle");
                      }
                  }
              });
          });
      },
      initComplete: function(settings, json) {
          console.log('datatable created');
          var table =  $('#datatable').DataTable();
          var oTable =  $('#datatable').dataTable();
      }
  });

  function load_csv(csv_file, element) {
    init_table({
      csv_path: csv_file,
      element: element,
    });
  }

  $('#template-modal').on('show.bs.modal', function (e) {
    $(this).addClass('modal-fullscreen');
    setTimeout( function() {
      $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    }, 0);
    $(this).find('.modal-body').append($('<div id="csv-table"></div>'))
    $(this).find('.modal-footer').hide();
    load_csv('job/download?csv=' + $(e.relatedTarget).data('csv-file'), 'csv-table');
  });

  $(".template-modal").on('hidden.bs.modal', function () {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
  });

}); // end on DOM ready
</script>
@endpush

@section('content')
  <div>
    <table class="table table-hover table-condensed table-bordered table-striped" id="datatable"></table>
  </div>
@stop
