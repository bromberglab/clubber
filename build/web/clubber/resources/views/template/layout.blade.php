<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- Scripts -->
        <!-- <script src="{{ asset('js/app.js') }}"></script> -->
        <script src="{{ URL::asset('template/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/fastclick-1.0.6.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/lodash-4.17.4.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/bootstrap-3.3.7.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/bootstrap-select-1.12.2.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/tether-1.4.0.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/datatables-1.10.13.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/datatables.buttons-1.2.4.min.js') }}"></script>
        <!-- <script src="https://cdn.datatables.net/select/1.2.1/js/dataTables.select.min.js"></script> -->
        <!-- <script src="//gyrocode.github.io/jquery-datatables-checkboxes/1.1.0/js/dataTables.checkboxes.min.js"></script> -->
        <script src="{{ URL::asset('template/js/jszip-3.1.3.min.js') }}"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.25/pdfmake.min.js"></script> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.25/vfs_fonts.js" integrity="sha256-Z4C2ncQ6qTBKGEjvLDWOAT1JIcxVJszOWm6Mt8GUUUs=" crossorigin="anonymous"></script> -->
        <script src="{{ URL::asset('template/js/buttons.html5-1.2.4.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/buttons.print-1.2.4.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/jquery.qtip-3.0.3.min.js') }}"></script>
        <!-- <script src="http://cytoscape.github.io/cytoscape.js/api/cytoscape.js-latest/cytoscape.min.js"></script> -->
        <!-- <script src="https://cdn.rawgit.com/cytoscape/cytoscape.js-qtip/2.2.5/cytoscape-qtip.js"></script> -->
        <script src="{{ URL::asset('template/js/d3-4.6.0.min.js') }}"></script>
        <!-- <script src="{{ URL::asset('vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script> -->
        <script src="{{ URL::asset('template/js/datatables.responsive-2.1.1.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/resumable.js') }}"></script>
        <script src="{{ URL::asset('template/js/bootstrap-waitingfor.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/html2canvas.js') }}"></script>
        <script src="{{ URL::asset('template/js/jquery.csv.min.js') }}"></script>
        <script src="{{ URL::asset('template/js/csv_to_html_table.js') }}"></script>
        <script src="{{ URL::asset('template/js/main.js') }}"></script>
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
        <script>
            $(function() { // begin on DOM ready
                // attach FastClick
                FastClick.attach(document.body);

                // setup ajax csrf toke
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                // enable bootstrap tooltips
                $('[data-toggle="tooltip"]').tooltip();
                $('.ttip').tooltip();

                // enable bootstrap popovers
                $('[data-toggle="popover"]').popover();

                var help_overlay = $('#main-help-wrapper');
                // toggle help overlay site wide
                $('#menu-help').click(function(){
                    help_overlay.removeClass().addClass('help-bg-{{ Request::segment(2) == 'example' ? 'example' : (Request::segment(1) == 'results' && Request::segment(2) == '' ? 'get-results' : Request::segment(1)) }}').fadeIn();
                });
                help_overlay.click(function(){
                    $(this).fadeOut();
                });

                originalY = help_overlay.offset().top;

                 // Space between element and top of screen (when scrolling)
                 var topMargin = 0;

                 $(window).on('scroll', function(event) {
                     var scrollTop = $(window).scrollTop();

                     help_overlay.stop(false, false).animate({
                         top: scrollTop < originalY
                                 ? 0
                                 : -1 * (scrollTop - originalY + topMargin)
                     }, 300);
                 });

                 // Retrieve data for modal from trigger element
                 $('#template-modal').on('show.bs.modal', function (event) {
                    var trigger = $(event.relatedTarget);
                    var content = trigger.data('modalcontent');
                    var title = trigger.data('modaltitle');
                    var modal = $(this);
                    modal.find('.modal-title').text(title);
                    // modal.find('.modal-body').text(content);
                    console.log($(content));
                    $(content).clone().children().appendTo(modal.find('.modal-body').empty());
                  });

            }); // end on DOM ready
        </script>
        @stack('scripts')
        <script src="{{ URL::asset('template/js/slick-1.6.0.min.js') }}"></script>
        <script>
            $(function() { // begin on DOM ready

              // initiate slick
              $('.slick').slick({
                infinite: false,
                accessibility: false,
                draggable: false
              });
              $('.slick-slide textarea').filter('[id=""]').each(function(i,v){v.name = v.name + '_slick'})

              // initiate bootstrap-select
              $('.selectpicker').selectpicker({
                container: 'body'
              });

            }); // end on DOM ready
        </script>
        <script src="{{ URL::asset('template/js/bootstrap-dialog-1.34.7.min.js') }}"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- CSS -->
        <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/bootstrap-3.3.7.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/bootstrap-theme-3.3.7.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/bootstrap-select-1.12.2.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/tether-1.4.0.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/font-awesome-4.7.0.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/datatables-1.10.13.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/buttons.datatables-1.2.4.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/select.datatables-1.2.1.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/datatables.checkboxes-1.1.0.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/jquery.qtip-3.0.3.min.css') }}" rel="stylesheet">
        <!-- <link href="{{ URL::asset('vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet"> -->
        <link href="{{ URL::asset('template/css/style.css') }}" rel="stylesheet">
        @stack('stylesheets')
        <link href="{{ URL::asset('template/css/slick-1.6.0.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/slick-theme-1.6.0.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/bootstrap-dialog-1.34.7.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('template/css/responsive.datatables-2.1.1.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">

    </head>
    <body>
        <div id="app">
            <!-- Layout Content  -->

            @section('help')
              <div id="main-help-wrapper" class="container title flex-center"></div>
            @stop

            @section('title')
            <div class='container title flex-center'>
                <em>{{ config('app.name') }}</em>
            </div>
            @stop

            @section('navbar')
            <!-- Insert according route into href of menu links.-->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a id="menu-home" class="navbar-brand" href="{{ route('home') }}">home</a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li id="menu-config" {{ Request::segment(1) === 'project' ? 'class=active' : '' }}><a href="{{ route('project.index') }}">Projects</a></li>
                            <li id="menu-config" {{ Request::segment(1) === 'project_job' ? 'class=active' : '' }}><a href="{{ route('project_job.index') }}">Submissions</a></li>
                            <li id="menu-config" {{ Request::segment(1) === 'job' ? 'class=active' : '' }}><a href="{{ route('job.index') }}">Jobs</a></li>
                            <li id="menu-config" {{ Request::segment(1) === 'cluster' ? 'class=active' : '' }}><a href="{{ route('cluster.index') }}">Clusters</a></li>
                            <li id="menu-config" {{ Request::segment(1) === 'logs' ? 'class=active' : '' }}><a href="{{ route('logs') }}">Logs</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li id="menu-about" {{ Request::segment(1) === 'about' ? 'class=active' : '' }}><a href="#">About</a></li>
                            <li id="menu-help"  {{ Request::segment(1) === 'help' ? 'class=active' : '' }}><a href="javascript:void(0)">Help</a></li>
                            <li><a href="mailto:services@bromberglab.org">Contact</a></li>
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-user-circle" aria-hidden="true"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            @role('Admin') {{-- Laravel-permission blade helper --}}
                                                <a href="{{ route('config') }}"><i class="fa fa-cogs" aria-hidden="true"></i> Configuration</a>
                                                <a href="{{ route('users.index') }}"><i class="fa fa-btn fa-unlock"></i> Admin</a>
                                            @endrole

                                            <a href="javascript:BootstrapDialog.show({title: 'API Token', type: BootstrapDialog.TYPE_INFO, message: '{{ Auth::user()->api_token }}'});"><i class="fa fa-key" aria-hidden="true"></i> API Token</a>

                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                <i class="fa fa-sign-out" aria-hidden="true"></i> Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            <li><a href="http://www.bromberglab.org"><img style="height:18px;" src="{{ URL::asset('template/img/logo_small_button.png') }}"/></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            @stop

            @section('modal')
              <div class="modal fade" id="template-modal" tabindex="-1" role="dialog" aria-labelledby="template-modal-label">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="template-modal-label">New message</h4>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                      <button id="template-modal-cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <button id="template-modal-submit" type="button" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            @stop

            @section('flash_message')
              @if(Session::has('flash_message'))
                  <div class="container">
                      <div class="alert alert-success"><em> {!! session('flash_message') !!}</em>
                      </div>
                  </div>
              @endif
            @stop

            @section('errors')
              <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                      @include ('errors.list') {{-- Including error file --}}
                  </div>
              </div>
            @stop

            @section('footer')
              <span>&#169; 2017 <a href='http://bromberglab.org'>Bromberg Lab</a> all rights reserved | Services are provided free of charge for academic and non-profit users. | <a href=mailto:services@bromberglab.org>contact us</a></span>
            @stop

            <!-- Layout Structure  -->

            <div id="layout-help">
              @yield('help')
            </div>

            <div id="layout-title">
              @yield('title')
            </div>

            <div id="layout-navbar" class='container' style="padding-top:10px">
              @yield('navbar')
            </div>

            <div id="layout-content" class='container'>
              @yield('content')
            </div>

            <div id="layout-form" class='container'>
              @yield('form')
            </div>

            <div id="layout-modal">
              @yield('modal')
            </div>

            <div id="layout-flash-message" style="margin-top:25px">
              @yield('flash_message')
            </div>

            <div id="layout-errors" class='container'>
              @yield('errors')
            </div>

            <div id="layout-footer" class='footer container content'>
              @yield('footer')
            </div>
        </div>
    </body>
</html>
