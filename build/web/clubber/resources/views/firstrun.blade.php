@extends('template.layout')

@push('scripts')
<script>
  $(function(){
    $('#form-config').on('submit',function(e){
      $.ajaxSetup({
          header:$('meta[name="_token"]').attr('content')
      })

      console.log('POST request: ' + this.id);
      var trigger = $( this );
      e.preventDefault();

      waitingDialog.show('Saving configuration & verifying cluster(s)...', {dialogSize: 'sm'});

      $.ajax({
        type:"POST",
        url:'{{ route('config_process_ajax') }}',
        data:$(this).serialize(),
        dataType: 'json',
        success: function(data){
          console.log('POST success: ' + trigger.attr('id'));
          console.log(data);
          if (data['errors'] == '') {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_SUCCESS,
                title: 'Success',
                message: 'Configuration saved!',
                closable: false,
                onshow: function(dialog) {
                  setTimeout(function() {
                    window.location.href = "{{ route('home') }}";
                  }, 1000);
                },
            });
          } else {
            BootstrapDialog.show({
              type: BootstrapDialog.TYPE_WARNING,
              title: 'Warning',
              message: data['errors'],
            });
          }
        },
        error: function(data){
          console.log('POST error: ' + trigger.attr('id'));
          var errors = data.responseJSON;
          var $msg = $('<ul/>');
          if (window['errors'] != undefined) {
            if (errors.length == 0) {
               $msg.append( $('<li>An error occured, please check your configuration or <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
            }
            _.forEach(errors, function(value, key) {
               $msg.append( $("<li>"+value+"</li>") );
            } )
          } else {
            $msg.append( $('<li>An error occured, please check your configuration or <b><a href="mailto:services@bromberglab.org">contact</a></b> us!</li>') );
          }
          BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            title: 'Error',
            message: $msg,
          });
        }
      })
      .always(function(data) {
            waitingDialog.hide();
      });
    });

    @if (!App\Project::find(1))
    $('#cluster').on('keyup', function(e){
      $('#config-submit').removeAttr('disabled');
    });
    $('#config-next').on('click', function(e){
      $('.slick').slick('slickNext');
      $(this).hide();
      $('#config-submit').show();
    });
    @endif
  });
</script>
@endpush

@section('content')
<div class="panel panel-default">
  <div class="panel-body"
      style="text-align: center; width: 95%; margin:
      auto;font-size:1.2em;position:relative">
    <div id="home-content">
      <div id='clubber-config'>
        {!! Form::open(['action' => 'IndexController@config_write_ajax', 'id' => 'form-config']) !!}
          <div id="clubber-config-wrapper">
            <p>
              <legend>First-run Configuration</legend>
            </p>
            <div class="alert alert-info">
              <p class="centered-description">
                At least one cluster is <b>required</b> to use <em>clubber</em>.<br/>
                Edit <em>Cluster SSH Connections</em> and <em>Cluster Configurations</em>.<br/>
                <em>Clubber Main Configuration</em> and <em>Database</em> may be left to default.
              </p>
            </div>
            <br/>
            <div class="slick">
              <div>
                <legend>Cluster SSH Connections</legend>
                <p class="centered-description">
                  <em>clubber</em> uses SSH to communicate with compute resources.<br/><br/>
                  <strong>Note:</strong> <em>MyCluster</em> represents the alias for your cluster.
                  The same alias has to be used in the next step <em>Cluster Configurations</em> to supply further details for your cluster.<br/>
                  Use <em>passwd</em> below for default username/password authentification.
                  <pre>
          MyCluster.hostname:  	...enter hostname / IP here...
          MyCluster.username:  	...enter username here...
          MyCluster.auth:  	[passwd|idrsa]
          MyCluster.password:  	...enter password here...
          MyCluster.port:  	22</pre>
                </p>
                <br/>
                <p class="centered-description">
                  <span class="label label-danger">required</span> Edit the SSH configuration here:
                </p>
                {{ Form::textarea('SSH', implode("\n", $config['SSH']), ['id' => 'ssh']) }}
                <br/>
              </div>
              <div>
                <legend>Cluster Configurations</legend>
                <p class="centered-description">
                  <em>clubber</em> is compatible with SGE and SLURM job managers.
                  For each cluster the correct job manager type, accessible project directory and maximum job queue limit has to be specified.<br/>
                  A <em>loadthreshold</em> option can be set at which no more jobs are submitted.<br/>
                  When the <em>ondemand</em> feature is enabled jobs are only submitted to the cluster if more jobs than the given threshold are currently queued.
                  <pre>
          MyCluster.manager:          [SGE|SLURM]
          MyCluster.basedir:          /path/to/my/project/directory
          MyCluster.queuelimit:       1000
          MyCluster.loadthreshold:    90%
          MyCluster.ondemand:         no
          MyCluster.ondemand.min:     500</pre>
                </p>
                <br/>
                <p class="centered-description">
                  <span class="label label-danger">required</span> Edit the Cluster configuration here:
                </p>
                {{ Form::textarea('cluster', implode("\n", $config['cluster']), ['id' => 'cluster']) }}
                <br/>
              </div>
              <div>
                <legend>Clubber Main Configuration</legend>
                <p class="centered-description">
                  Specify here which database and list of clusters to use.<br/>
                  <em>database:</em> the MySQL database to use<br/>
                  <em>enabled:</em> comma seperated list of clusters (aliases) to use<br/>
                  <em>arrayjob.size:</em> number of jobs submitted in parallel per cluster<br/>
                  <em>autobal.timer.sec:</em> interval in seconds between auto balancing<br/>
                  <em>max.job.restarts:</em> maximum restarts due to job errors<br/>
                  <pre>
                  timezone:               +01:00
                  database:               default_db
                  enabled:                MyCluster
                  arrayjob.size:          50
                  autobal.timer.sec:      60
                  max.job.restarts:       3</pre>
                </p>
                <br/>
                <p class="centered-description">
                  <span class="label label-info">optional</span> Edit the Main configuration here:
                </p>
                {{ Form::textarea('clubber', implode("\n", $config['clubber']), ['id' => 'clubber']) }}
                <br/>
              </div>
              <div>
                <legend>Database</legend>
                <p class="centered-description">
                  <em>clubber</em> uses a MySQL 5.x database. By default the MySQL database provided by the docker container is used:
                  <pre>
                default_db.hostname:  	127.0.0.1
                default_db.database:  	clubber
                default_db.username:  	clubber
                default_db.password:  	clubber
                default_db.port    :  	3306</pre>
                </p>
                <br/>
                <p class="centered-description">
                  <span class="label label-info">optional</span> Edit the MySQL configuration here:
                </p>
                {{ Form::textarea('Database', implode("\n", $config['Database']), ['id' => 'database']) }}
                <br/>
              </div>
            </div>
            <div>
              {{ Form::submit('Save Config', ['id' => 'config-submit', 'class' => 'btn btn-primary', 'style' => App\Project::find(1) ? '' : 'display: none;', App\Project::find(1) ? '' : 'disabled']) }}
              @if (!App\Project::find(1))
                <button id="config-next"type="button" class="btn btn-default">next</button>
              @endif
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop
