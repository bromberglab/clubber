@extends('template.layout')

@section('content')
<div class="panel panel-default">
  <div class="panel-body"
      style="text-align: center; width: 95%; margin:
      auto;font-size:1.2em;position:relative">
    <div id="home-content">
      <h2>Welcome to CLuBBeR</h2>
      <h4>Cluster Load Balancer for Bioinformatics e-Resources</h4>
      <br/>
      <p>
        With the advent of modern day high-throughput technologies, the bottleneck in biological discovery has shifted from the cost of doing experiments to that of analyzing their results. clubber is our automated cluster load balancing system developed for optimizing these “big data” analyses. Its plug-and-play framework encourages re-use of existing solutions for bioinformatics problems. clubber’s goals are to reduce computation times and to facilitate use of cluster computing. The first goal is achieved by automating the balance of parallel submissions across available high performance computing (HPC) resources. Notably, the latter can be added on demand, including cloud-based resources, and/or featuring heterogeneous environments. The second goal of making HPCs user-friendly is facilitated by an interactive web interface and a RESTful API allowing for job monitoring and result retrieval. We used clubber to speed up our pipeline for annotating molecular functionality of metagenomes. We analyzed the Deepwater Horizon oil-spill study data to quantitatively show that that the beach sands have not yet entirely recovered. Further, our analysis of the CAMI-challenge data revealed that microbiome taxonomic shifts do not necessarily correlate with functional shifts. These examples (21 metagenomes processed in 172 minutes) clearly illustrate the importance of clubber in the everyday computational biology environment.
      </p>
      <br/>
      <h5>Online Resources</h5>
      <p>
        Docker hub: <a href="https://hub.docker.com/r/bromberglab/clubber/">https://hub.docker.com/r/bromberglab/clubber/</a>
      </p>
      <p>
        Git repository: <a href="https://bitbucket.org/bromberglab/bromberglab-clubber/">https://bitbucket.org/bromberglab/bromberglab-clubber/</a>
      </p>
      <p>
        Reference: <a href="#">Published May 16, 2017	| 0, 20170020 (2017) | DOI: 10.1515/jib-2017-0020 | Journal of Integrative Bioinformatics</a>
      </p>
      <br/>
      <div class="btn-group btn-group-lg" role="group" aria-label="clubber-tools">
          <a href="{{ route('project.index') }}" role="button" class="btn btn-default">
            Create new Project
          </a>
          <a href="{{ route('project_job.index') }}" role="button" class="btn btn-default">
            Submit new Job
          </a>
          <a href="{{ route('job.index') }}" role="button" class="btn btn-default">
            List Jobs
          </a>
      </div>
      <br/>
      <br/>
    </div>
  </div>
</div>
@stop
