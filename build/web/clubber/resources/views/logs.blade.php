@extends('template.layout')

@push('scripts')
<script>
$(function() { // begin on DOM ready
    var $textarea = $('#logs');
    $textarea.scrollTop($textarea[0].scrollHeight);
}); // end on DOM ready
</script>
@endpush

@section('content')
<div class="panel panel-default">
  <div class="panel-body"
      style="text-align: center; width: 95%; margin:
      auto;font-size:1.2em;position:relative">
    <div id="home-content">
      <div class="panel-body"
          style="text-align: center; width: 100%; margin:
          auto;font-size:1.2em;position:relative">
          {{ Form::textarea('logs', $logs, ['id' => 'logs', 'style' => 'height:500px;']) }}
      </div>
    </div>
  </div>
</div>
@stop
