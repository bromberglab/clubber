<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;

use Storage;
use Auth;
use App\Project;
use App\User;
use App\ProjectJob;
use Yajra\Datatables\Facades\Datatables;

class Project_JobController extends Controller
{

    public function __construct() {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        if ($user->hasRole('Admin')) {
          $projects = Project::all();
        } else {
          $projects = collect();
          foreach (Project::all() as $project) {
           if ($user->hasPermissionTo('access project ' . $project->id)) {
              $projects->push($project);
           }
          }  
        }

        $projectid2name = [];
        $project_job_details = [];
        foreach ($projects->sortBy('name')->values()->all() as $project)
        {
            $projectid2name[$project->id] = $project->name;
            $project_job_details[$project->id] = [
              $project->pre_processor,
              $project->post_processor
            ];
        }
        return view('project_job.index', [
          'projectid2name' => $projectid2name,
          'project_job_details' => $project_job_details
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project_job = new ProjectJob;
        $project_job->project = $request->project;

        $arguments_file_name = 'arguments.list';
        if ($request->has('project_job_arguments')) {
            $arguments_list = trim($request->project_job_arguments) . "\n";
            $project_job->args = $arguments_file_name;
        } elseif ($request->hasFile('project_job_argument_file')) {
            $arguments_file = $request->file('project_job_argument_file');
             //$arguments_file->getClientOriginalName();
            $project_job->args = $arguments_file_name;
        }

        if ($request->hasFile('project_job_file')) {
            $jobfile = $request->file('project_job_file');
            $jobfile_name = $jobfile->getClientOriginalName();
            $project_job->jobfile = $jobfile_name;
        }

        // save in database
        $project_job->save();
        $project = Project::find($project_job->project);

        if ($request->hasFile('project_job_file')) {
            $jobfile_path = $jobfile->storeAs('project_job_jobfiles/' . $project->id .'/'. $project_job->id, $jobfile_name);
        }

        if ($request->has('project_job_arguments')) {
            Storage::put(
                'project_job_arguments/' . $project->id .'/'. $project_job->id .'/'. $arguments_file_name,
                $arguments_list
            );
        } elseif ($request->hasFile('project_job_argument_file')) {
            $arguments_file_path = $arguments_file->storeAs('project_job_arguments/' . $project->id .'/'. $project_job->id, $arguments_file_name);
        }

        // execute clubber
        $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a create_project_job' . ' --proj_id ' . $project_job->project . ' --projob_id ' . $project_job->id;
        // debug:
        // print($cmd);
        $process = new Process($cmd);
        $process->run();
        if (!$process->isSuccessful()) {
            return($process->getOutput()); // throw new ProcessFailedException($process);
        }
        // $process->getOutput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project_job = ProjectJob::find($id);

        // execute clubber
        $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a delete_project_job' . ' --proj_id ' . $project_job->project . ' --projob_id ' . $project_job->id;
        $process = new Process($cmd);
        $process->run();
        if (!$process->isSuccessful()) {
            return($process->getOutput()); // throw new ProcessFailedException($process);
        }
        // $process->getOutput();

        // delete from database
        ProjectJob::destroy($id);
    }

    /**
     * Create datatables for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function datatables($id = NULL)
    {
      $user = User::find(Auth::id());
      if ($user->hasRole('Admin')) {
        return Datatables::of(ProjectJob::all())->make(true);
      }
      $project_jobs = [];
      foreach (ProjectJob::all() as $project_job) {
       if ($user->hasPermissionTo('access project ' . $project_job->project)) {
          array_push($project_jobs, $project_job);
       }
      }
      return Datatables::of($project_jobs)->make(true);
    }

    /**
     * Return model attributes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function model($id = NULL)
     {
       $project_job = ProjectJob::find($id);
       dd($project_job->getAttributes());
       return $project_job->getAttributes();
     }

     /**
      * Download the specified resource.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function download(Request $request, $id = NULL)
     {
         if ($request->zipped) {
           $path = $request->zipped;
         } else {
           $project = Project::find($request->project);
           $idxc = new IndexController();
           $config = $idxc->config_parser();
           $local_outdir = $config['clubber']['local.output.dir'];
           $path = config('clubber.wd') . '/' . $project->name . '/' . $request->target . '/' . $local_outdir;
           $content_type = $request->content_type;
         }

         if ( !file_exists($path) and $request->target != ""  )
         {
             return response()->json(['error' => 'No results available for this submission.'], 404);
         }

         if ($request->action == 'check')
         {
              $response_content = '<a href="' . route('project_job.download') . '?zipped=' . $path . '" class="list-group-item list-group-item-action" data-action="' . 'zipped' . '"><i class="fa fa-file file-download-listitem" aria-hidden="true"></i>Submission results (zipped)</a>';

              $response_body =
              '<div class="list-group">' .
              '<h5>Available Downloads for <em>Submission' . $request->target . '</em></h5>' .
              $response_content .
              '</div>';

              return $response_body;
         }

         if ($request->action == 'zipped' || $request->zipped)
         {
            $zipfile  = sprintf('%s.zip', storage_path('app/tmp/' . str_random(8)));
            $this->zipData($path, $zipfile);
            $content_type = 'application/octet-stream'; //'application/download';
            $filename = $zipfile;
            $realFileName = $request->zipped ? basename($request->zipped) : basename($request->target);
            $download_name = sprintf('%s.zip', $realFileName);
         } else {
             $filename = $path;
             $download_name = basename($request->target);
         }


         //$handle = fopen($filename, 'r');

         $headers = array(
             'Content-Type' =>  $content_type,
         );

         $response = response()->download($filename, $download_name, $headers);
         $response->deleteFileAfterSend(true);
         return $response;
     }

     public function zipData($source, $destination) {
         if (extension_loaded('zip') === true) {
             if (file_exists($source) === true) {
                 $zip = new \ZipArchive();
                 if ($zip->open($destination, \ZIPARCHIVE::CREATE) === true) {
                     $source = realpath($source);
                     if (is_dir($source) === true) {
                         $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
                         foreach ($files as $file) {
                             $file = realpath($file);
                             if (is_dir($file) === true) {
                                 $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                             } else if (is_file($file) === true) {
                                 $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                             }
                         }
                     } else if (is_file($source) === true) {
                         $zip->addFromString(basename($source), file_get_contents($source));
                     }
                 }
                 return $zip->close();
             }
         }
         return false;
     }
}
