<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;

class ClusterController extends Controller
{

    public function __construct() {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idxc = new IndexController();
        $config = $idxc->config_parser();
        $clusters_enabled = $config['clubber']['enabled'];
        $clusters = [];
        foreach (explode(',', $clusters_enabled) as $cluster)
        {
          $cluster_log = storage_path('logs/' .$cluster . '.log');

          $clusters[$cluster] = [ 'title' => $cluster, 'updated' => 'NA', 'workload' => '- ', 'storage' => '- ', 'shell' => 'NA', 'connection' => 'NA'];

          // if (!file_exists($cluster_log)) {
          //   $this->update(request(), $cluster);
          // }

          $clusters[$cluster] = $this->show($cluster);
        }
        return view('clusters', [ 'clusters' => $clusters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cluster_log = storage_path('logs/' .$id . '.log');

        $cluster = [ 'title' => $id, 'updated' => 'NA', 'workload' => '- ', 'storage' => '- ', 'shell' => 'NA', 'connection' => 'NA', 'connection_msg' => NULL];

        $log = json_decode(file_get_contents($cluster_log), TRUE);
        if (count($log['errors']) > 0 || !array_key_exists('connection', $log)) {
          $cluster['updated'] = date('h:m:s d.m.Y', $log['timestamp']);
          $cluster['connection'] = 'failed';
          $cluster['connection_msg'] = $log['errors'];
        } else {
          $workload = ($log['workload']['cpus_summary']['total'] - $log['workload']['cpus_summary']['idle'])/$log['workload']['cpus_summary']['total'];
          $cluster = [
            'title' => $id,
            'updated' => date('h:m:s d.m.Y', $log['timestamp']),
            'workload' => number_format($workload*100, 2) . '% of ' . $log['workload']['cpus_summary']['total'] . ' cores',
            'storage' => $log['storage']['Use%'] . ' of ' . $log['storage']['Size'],
            'shell' => $log['shell'],
            'connection' => $log['connection'] == 'success' ? 'ok' : 'failed',
            'connection_msg' => $log['connection'] == 'success' ? NULL : $log['connection']
          ];
        }
        return $cluster;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $timestamp = time();
      $cluster_log = storage_path('logs/' .$id . '.log');

      $actions = 'workload,storage,shell,connection';
      // execute clubber
      $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a cluster_info' . ' --cluster_alias ' . $id . ' --cluster_actions ' . $actions . ' 2> /dev/null' ;
      $process = new Process($cmd);
      $process->run();
      if (!$process->isSuccessful()) {
          return($process->getOutput()); // throw new ProcessFailedException($process);
      }
      $out = $process->getOutput();
      $out_lines = explode(PHP_EOL, $out);
      $log = json_decode($out_lines[count($out_lines)-2], true);
      $log['timestamp'] = $timestamp;
      file_put_contents($cluster_log, json_encode($log));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function refresh(Request $request, $id)
    {
      $this->update($request, $id);
      return $this->show($id);
    }
}
