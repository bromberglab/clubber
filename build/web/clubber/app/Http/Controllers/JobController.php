<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;

use Auth;
use App\Project;
use App\User;
use App\ProjectJob;
use App\Job;
use Yajra\Datatables\Facades\Datatables;

class JobController extends Controller
{

    public function __construct() {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $projectid2name = [];
        foreach ($projects as $project)
        {
          $projectid2name[$project->id] = $project->name;
        }
        return view('job.index', [ 'projectid2name' => $projectid2name]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      switch ($request->action) {
          case "restart":
              // $job = Job::find($id);
              // $job->status = 0;
              // $job->daemon = 0;
              // $job->save();

              // execute clubber
              $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a restart_job' . ' --proj_id ' . $job->project . ' --projob_id ' . $job->project_job . ' --job_id ' . $job->id;
              $process = new Process($cmd);
              $process->run();
              if (!$process->isSuccessful()) {
                  return($process->getOutput()); // throw new ProcessFailedException($process);
              }
              // $process->getOutput();
              break;

          default:
              //
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Create datatables for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function datatables($id = NULL)
    {
      $user = User::find(Auth::id());
      if ($user->hasRole('Admin')) {
        return Datatables::of(Job::all())->make(true);
      }
      $jobs = [];
      foreach (Job::all() as $job) {
       if ($user->hasPermissionTo('access project ' . $job->project)) {
          array_push($jobs, $job);
       }
      }
      return Datatables::of($jobs)->make(true);
    }

    /**
     * Return model attributes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function model($id = NULL)
     {
       $job = Job::find($id);
       dd($job->getAttributes());
       return $job->getAttributes();
     }

     /**
      * Download the specified resource.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function download(Request $request, $id = NULL)
     {
         if ($request->zipped) {
           $path = $request->zipped;
         } elseif ($request->csv) {
           $path = $request->csv;
           $content_type = 'application/octet-stream';
         } else {
           $project = Project::find($request->project);
           $project_job_id = $request->project_job;
           $idxc = new IndexController();
           $config = $idxc->config_parser();
           $local_outdir = $config['clubber']['local.output.dir'];
           $path = config('clubber.wd') . '/' . $project->name . '/' . $project_job_id . '/' . $local_outdir . '/' . $request->target; // . '/' . $project->transfer_dir;
           $content_type = $request->content_type;
         }

         if ( !file_exists($path) and $request->target != ""  )
         {
             return response()->json(['error' => 'No results available for this job.'], 404);
         }

         if ($request->action == 'check')
         {
              $response_content = '<a href="' . route('job.download') . '?zipped=' . $path . '" class="list-group-item list-group-item-action" data-action="' . 'zipped' . '"><i class="fa fa-file file-download-listitem" aria-hidden="true"></i>' . $request->target . '.zip</a>';

              $response_body =
              '<div class="list-group"><br/>' .
              // '<h5>Available Downloads for <em>Job ' . $request->target . '</em></h5>' .
              $response_content .
              '</div>';

              return $response_body;
         }

         if ($request->action == 'zipped' || $request->zipped)
         {
            $zipfile  = sprintf('%s.zip', storage_path('app/tmp/' . str_random(8)));
            $this->zipData($path, $zipfile);
            $content_type = 'application/octet-stream'; //'application/download';
            $filename = $zipfile;
            $realFileName = $request->zipped ? basename($request->zipped) : basename($request->target);
            $download_name = sprintf('%s.zip', $realFileName);
         } else {
             $filename = $path;
             $download_name = basename($request->target);
         }


         //$handle = fopen($filename, 'r');

         $headers = array(
             'Content-Type' =>  $content_type,
         );

         $response = response()->download($filename, $download_name, $headers);
         $response->deleteFileAfterSend(true);
         return $response;
     }

     /**
      * Check for csv in results and serve if available.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function csv(Request $request, $id = NULL)
     {
         $job = Job::find($id);
         $project = Project::find($job->project);
         $idxc = new IndexController();
         $config = $idxc->config_parser();
         $local_outdir = $config['clubber']['local.output.dir'];
         $source = config('clubber.wd') . '/' . $project->name . '/' . $job->project_job . '/' . $local_outdir . '/' . $job->jobid . '.' . $job->arrayidx . '/' . $project->transfer_dir;
         $source = realpath($source);
         if (is_dir($source) === true) {
             $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
             foreach ($files as $file) {
                 $file = realpath($file);
                 if (is_dir($file) === true) {
                     //
                 } else if (is_file($file) === true) {
                     $ext = pathinfo($file, PATHINFO_EXTENSION);
                     if ($ext == 'csv') {
                       return $file;
                     }
                 }
             }
         } else if (is_file($source) === true) {
             $ext = pathinfo($file, PATHINFO_EXTENSION);
             if ($ext == 'csv') {
               return file;
             }
         }

         return null;
     }

     /**
      * Compress results for download.
      *
      */
     public function zipData($source, $destination) {
         if (extension_loaded('zip') === true) {
             if (file_exists($source) === true) {
                 $zip = new \ZipArchive();
                 if ($zip->open($destination, \ZIPARCHIVE::CREATE) === true) {
                     $source = realpath($source);
                     if (is_dir($source) === true) {
                         $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::SELF_FIRST);
                         foreach ($files as $file) {
                             $file = realpath($file);
                             if (is_dir($file) === true) {
                                 $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                             } else if (is_file($file) === true) {
                                 $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                             }
                         }
                     } else if (is_file($source) === true) {
                         $zip->addFromString(basename($source), file_get_contents($source));
                     }
                 }
                 return $zip->close();
             }
         }
         return false;
     }

}
