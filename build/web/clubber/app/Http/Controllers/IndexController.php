<?php

namespace App\Http\Controllers;

use App\Project;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function __construct() {
        $this->middleware(['auth']);
    }

     /**
     * Display home.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $project = Project::find(1);
        if (!$project) {
            return $this->config_read();
        }
        else
            return view('home');
    }

    /**
    * Display config.
    *
    * @return \Illuminate\Http\Response
    */
    public function config_parser($simple = 0)
    {
      $clubber_config = '/home/clubber/bromberglab_suite/python/config/main.conf';
      $config = [];
      $fn = fopen($clubber_config,"r");
      $section = NULL;
      while(! feof($fn)) {
          $result = rtrim(fgets($fn));
          if(substr($result, 0, 1) == "[" && substr($result, -1, 1) == "]")
              $section = substr($result, 1, -1);
          else {
              if (!array_key_exists($section, $config))
                  $config[$section] = [];
              switch ($section) {
                  case "Database":
                      if( $simple == 0 and strpos( $result, ':' ) !== false ) {
                          $config[$section][trim(explode(':', $result)[0])] = trim(explode(':', $result)[1]);
                      } else if ($simple == 1) {
                        array_push($config[$section], trim($result));
                      }
                      break;
                  case "SSH":
                      if( $simple == 0 and strpos( $result, ':' ) !== false ) {
                          $config[$section][trim(explode(':', $result)[0])] = trim(explode(':', $result)[1]);
                      } else if ($simple == 1) {
                        array_push($config[$section], trim($result));
                      }
                      break;
                  case "clubber":
                      if( $simple == 0 and strpos( $result, ':' ) !== false ) {
                          $config[$section][trim(explode(':', $result)[0])] = trim(explode(':', $result)[1]);
                      } else if ($simple == 1) {
                        array_push($config[$section], trim($result));
                      }
                      break;
                  case "cluster":
                      if( $simple == 0 and strpos( $result, ':' ) !== false ) {
                          $config[$section][trim(explode(':', $result)[0])] = trim(explode(':', $result)[1]);
                      } else if ($simple == 1) {
                        array_push($config[$section], trim($result));
                      }
                      break;

                  default:
                      if( $simple == 0 and strpos( $result, ':' ) !== false ) {
                          $config[$section][trim(explode(':', $result)[0])] = trim(explode(':', $result)[1]);
                      } else if ($simple == 1) {
                        array_push($config[$section], trim($result));
                      }
              }
          }
      }
      fclose($fn);
      // dd($config);
      return $config;
    }

    /**
    * Display config.
    *
    * @return \Illuminate\Http\Response
    */
    public function config_read()
    {
        $config = $this->config_parser(1);
        return view('firstrun', ['config' => $config]);
    }

    /**
    * Write config.
    *
    * Using default form post request (NO AJAX)
    *
    * @return \Illuminate\Http\Response
    */
    public function config_write(Request $request)
    {
      $config_current = $this->config_parser(1);
      $config_edited = $request->request->all();
      $config_new = '';
      // dd($config_edited );
      foreach ($config_current as $section => $content)
      {
        if ($section)
          $config_new .= "\n" . "[" . $section . "]" . "\n";
        if (array_key_exists($section, $config_edited))
        {
          $config_new .= $config_edited[$section]. "\n";
        }
        else {
          $config_new .= implode("\n", $content);
        }
      }
      $config_new = str_ireplace("\x0D", "", $config_new);
      file_put_contents("/home/clubber/bromberglab_suite/python/config/main.conf", $config_new);
    }

    /**
    * Write config.
    *
    * Using AJAX form post request
    *
    * @return \Illuminate\Http\Response
    */
    public function config_write_ajax(Request $request)
    {
      $this->validate($request, [
        // 'title' => 'bail|required|unique:posts|max:255',
        'Database' => 'required',
        'SSH' => 'required',
        'clubber' => 'required',
        'cluster' => 'required',
      ]);

      $config_current = $this->config_parser(1);
      $config_edited = $request->request->all();
      $config_new = '';
      // dd($config_edited );
      foreach ($config_current as $section => $content)
      {
        if ($section)
          $config_new .= "\n" . "[" . $section . "]" . "\n";
        if (array_key_exists($section, $config_edited))
        {
          $config_new .= $config_edited[$section] . "\n";
        }
        else {
          $config_new .= implode("\n", $content);
        }
      }
      $config_new = str_ireplace("\x0D", "", $config_new);
      file_put_contents("/home/clubber/bromberglab_suite/python/config/main.conf", $config_new);

      $config_updated = $this->config_parser();

      $db_config = $config_updated['Database'];
      $db_default = $config_updated['clubber']['database'];
      $db_mapping = [
        "DB_HOST" => "hostname",
        "DB_PORT" => "port",
        "DB_DATABASE" => "database",
        "DB_USERNAME" => "username",
        "DB_PASSWORD" => "password"
      ];
      foreach ($db_mapping as $key => $value)
      {
        $db_value = $db_config["{$db_default}.{$value}"];
        system("sed  -ie  's/\({$key}=\)\(.*\)/\\1{$db_value}/' " . base_path('.env'));
      }

      $project = Project::find(1);
      if (!$project) {
        $project = new Project;
        $project->name = 'default';
        $project->description = 'This default project is assigned to all jobs submitted without associated project';
        $project->script = 'clubber.sh';
        $project->save();
      }

      $clusters = [];
      foreach ($config_updated['cluster'] as $key => $value) {
        $key_ = explode('.', $key);
        if ($key_ && $key_[count($key_)-1] == "manager") {
          $clusters[$key_[0]] = NULL;
        }
      }

      $errors = '';
      foreach ($clusters as $id => $value) {
        $cluster_log = storage_path('logs/' .$id . '.log');

        $actions = 'connection';
        // execute clubber
        $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a cluster_info' . ' --cluster_alias ' . $id . ' --cluster_actions ' . $actions . ' 2> /dev/null' ;
        $process = new Process($cmd);
        $process->run();
        if (!$process->isSuccessful()) {
            return($process->getOutput()); // throw new ProcessFailedException($process);
        }
        $out = $process->getOutput();
        $out_lines = explode(PHP_EOL, $out);
        $log = json_decode($out_lines[count($out_lines)-2], true);
        if (count($log['errors']) > 0) {
          $errors .= '<b>Could not connect to cluster ' . $id . ':</b> <em>' . join('</br>', $log['errors']) . '</em></br>';
        }
      }

      return response()->json(['errors' => $errors]);
    }
}
