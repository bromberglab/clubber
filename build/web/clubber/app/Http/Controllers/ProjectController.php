<?php

namespace App\Http\Controllers;

use Storage;
use File;
use Validator;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;

use Auth;
use App\Project;
use App\User;
use Spatie\Permission\Models\Permission;
use Yajra\Datatables\Facades\Datatables;

class ProjectController extends Controller
{

    public function __construct() {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idxc = new IndexController();
        $config = $idxc->config_parser();
        $enabled_clusters = ['' => ''];
        foreach (explode(',', $config['clubber']['enabled']) as $cluster)
            $enabled_clusters[$cluster] = $cluster;

        $pre_processors = ['' => ''];
        $pre_processors_scripts = Storage::files('/project_pre_processors/');
        foreach ($pre_processors_scripts as $script)
        {
            $script_name = basename($script);
            $pre_processors[$script_name] = $script_name;
        }

        $post_processors = ['' => ''];
        $post_processors_scripts = Storage::files('/project_post_processors/');
        foreach ($post_processors_scripts as $script)
        {
            $script_name = basename($script);
            $post_processors[$script_name] = $script_name;
        }

        return view('project.index', [
          'enabled_clusters' => $enabled_clusters,
          'pre_processors' => $pre_processors,
          'post_processors' => $post_processors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
          'project_name' => 'required',
          //'project_script' => 'required',
          'project_cluster' => 'required',
        ], [
          'project_cluster.required' => 'At least one cluster has to be selected.',
        ])->validate();

        $project = new Project;
        $project->name = $request->project_name;
        $project->description = $request->project_description;

        $transfer = $request->project_transfer_switch == "1" ? 1 : 0;
        if ($transfer === 1) {
            if ($request->project_transferdir) {
              $project->transfer_dir = $request->project_transferdir;
            } else {
              $idxc = new IndexController();
              $config = $idxc->config_parser();
              $project->transfer_dir = $config['clubber']['remote.transfer.dir'];
            }
        }

        $dbstore = $request->project_dbstore_switch == "1" ? 1 : 0;
        $project->dbstore = $dbstore;

        //if ($request->has('project_cluster') && $request->project_cluster != '0')
            $project->valid_clusters = implode(',', $request->project_cluster);

        if ($request->hasFile('project_script')) {
            $script = $request->file('project_script');
            $script_name = $script->getClientOriginalName();
            $project->script = $script_name;
        }

        if ($request->hasFile('project_sources')) {
            $sources = $request->file('project_sources');
            $sources_name = $sources->getClientOriginalName();
            $project->sources = $sources_name;
        }

        if ($request->has('pre_processor')) {
            $project->pre_processor = $request->pre_processor;
        }

        if ($request->has('post_processor')) {
            $project->post_processor = $request->post_processor;
        }

        // save in database
        $project->save();

        if ($request->hasFile('project_script')) {
            $script_path = $script->storeAs('project_script_files/' . $project->id, $script_name);
        }

        if ($request->hasFile('project_sources')) {
            $sources_path = $sources->storeAs('project_source_files/' . $project->id, $sources_name);
        }

        foreach ($request->project_cluster as $cluster_name) {
            $cluster_parameters = trim( $request->input('cluster_'.$cluster_name.'_parameters') ) . "\n";
            Storage::put(
                'project_cluster_parameters/' . $project->id . '/' . $cluster_name . '.args',
                $cluster_parameters
            );
        }

        Permission::create(['name' => 'access project ' . $project->id]);

        // execute clubber
        $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a create_project' . ' --proj_id ' . $project->id;
        $process = new Process($cmd);
        $process->run();
        if (!$process->isSuccessful()) {
            return($process->getOutput()); // throw new ProcessFailedException($process);
        }
        // $process->getOutput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // execute clubber
        $cmd = 'sudo -u clubber ' . config('clubber.run') . ' -a delete_project' . ' --proj_id ' . $id;
        $process = new Process($cmd);
        $process->run();
        if (!$process->isSuccessful()) {
            return($process->getOutput()); // throw new ProcessFailedException($process);
        }
        // $process->getOutput();

        // delete from database
        Project::destroy($id);
    }

    /**
     * Create datatables for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function datatables($id = NULL)
    {
        $user = User::find(Auth::id());
        if ($user->hasRole('Admin')) {
          return Datatables::of(Project::all())->make(true);
        }
        $projects = [];
        foreach (Project::all() as $project) {
         if ($user->hasPermissionTo('access project ' . $project->id)) {
            array_push($projects, $project);
         }
        }
        return Datatables::of($projects)->make(true);
    }

    /**
     * Return model attributes.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function model($id = NULL)
     {
       $project = Project::find($id);
       dd($project->getAttributes());
       return $project->getAttributes();
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store_processing(Request $request)
     {
       if ($request->hasFile('preprocessing_script')) {
           $script = $request->file('preprocessing_script');
           $script_name = $script->getClientOriginalName();
           $script_path = $script->storeAs('project_pre_processors', $script_name);
           return response()->json(['msg' => 'Pre-processor has been added and is now available when creating a new project!']);
       }

       if ($request->hasFile('postprocessing_script')) {
           $script = $request->file('postprocessing_script');
           $script_name = $script->getClientOriginalName();
           $script_path = $script->storeAs('project_post_processors', $script_name);
           return response()->json(['msg' => 'Post-processor has been added and is now available when creating a new project!']);
       }
     }

     /**
      * Edit permissions.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function change_permissions(Request $request)
     {
       $enabled_users = [];
       $all_users = User::all()->pluck('id')->toArray();
       $project_id = $request->input('project_id');
       $project_permissions_update = $request->input('project_permissions');
       if (!is_null($project_permissions_update)) {
         foreach ($project_permissions_update as $user_id)
         {
            array_push($enabled_users, $user_id);
            $user = User::find($user_id);
            if (!$user->hasPermissionTo('access project ' . $project_id)) {
              $user->givePermissionTo('access project ' . $project_id);
            }
         }
       }
       foreach($all_users as $user_id) {
          if (!in_array($user_id, $enabled_users)) {
            $user = User::find($user_id);
            if (!$user->hasRole('Admin') && $user->id != Auth::id()) {
              $projects_owned = Project::where('user', '=', $user->id)->pluck('id')->toArray();
              if (!in_array($project_id, $projects_owned)) {
                if ($user->hasPermissionTo('access project ' . $project_id)) {
                  $user->revokePermissionTo('access project ' . $project_id);
                }
              }
            }
          }
       }
       return back();
     }
}
