<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
  /**
   * The name of the "created at" column.
   *
   * @var string
   */
  const CREATED_AT = 'created';

  /**
   * The name of the "updated at" column.
   *
   * @var string
   */
  const UPDATED_AT = 'updated';
  
}
