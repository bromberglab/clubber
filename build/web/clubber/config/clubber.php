<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    */

    'run' => '/home/clubber/bromberglab_suite/run.sh bblsuite/hpc/webservice.py',
    'wd' => '/home/clubber/wd',
];
