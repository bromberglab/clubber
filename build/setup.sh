#!/usr/bin/env bash

##-------------------------------------------------------
# UPDATE CONFIG FILES
##-------------------------------------------------------

# set UTF-8 environment
echo 'LC_ALL=en_US.UTF-8' >> /etc/environment
echo 'LANG=en_US.UTF-8' >> /etc/environment
echo 'LC_CTYPE=en_US.UTF-8' >> /etc/environment

# change mysql server to listen to more than localhost, connect from outside
sed -i -e"s/^bind-address\s*=\s*127.0.0.1/explicit_defaults_for_timestamp = true\nbind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf

# enable xdebug
echo 'xdebug.remote_enable=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_connect_back=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.show_error_trace=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.remote_port=9000' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.scream=0' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.show_local_vars=1' >> /etc/php/7.0/mods-available/xdebug.ini
echo 'xdebug.idekey=PHPSTORM' >> /etc/php/7.0/mods-available/xdebug.ini

# setup php7.0-fpm to not run as daemon (allow my_init to control)
sed -i "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.0/fpm/php-fpm.conf
sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.0/fpm/php.ini

# create run directories
mkdir -p /var/run/php
chown -R www-data:www-data /var/run/php
mkdir -p /var/run/mysqld
chown -R mysql:mysql /var/run/mysqld
mkdir -p /var/run/clubber
chown -R clubber:clubber /var/run/clubber

# initiate mysql database
echo '[mysqld]' >> /etc/mysql/my.cnf
echo 'sql-mode = STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'  >> /etc/mysql/my.cnf
mysqld --initiate
service mysql start
service mysql stop
