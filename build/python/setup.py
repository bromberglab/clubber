#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='clubber',
    version='2.0',
    description='CLUster load Balancer for Bioinformatics E-Resources',
    long_description='clubber is an cluster load balancer designed specifically to facilitate and accelerate common '
                     'computational biology experimental workflows and used in conjunction with existing methods or '
                     'scripts to efficiently process large-scale datasets. ',
    url='https://bitbucket.org/bromberglab/clubber.git',
    author='Maximilian Miller',
    author_email='mmiller@bromberglab.org',
    license='MIT',
    platforms='various',
    packages=find_packages(),
    install_requires=[
        'paramiko', ' mysqlclient'
    ],
)
