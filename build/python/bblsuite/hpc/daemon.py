#!/usr/bin/python
import argparse
import os.path
import datetime
import time
import sys

from bblsuite.helper import mysql, config
from bblsuite.hpc import run
from bblsuite.hpc.daemonbase.daemonpy3 import DaemonPy3

"""Daemon for hpc submitting and load balancing"""

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Daemon(DaemonPy3):
    """Daemon inherits generic Daemon (python3)"""

    def __init__(self, pidfile, db_handler):
        DaemonPy3.__init__(self, pidfile)

        self.db = db_handler

    def run(self):
        """Run daemon"""

        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        env_umask = os.umask(0o002)
        self.log.info("Deamon started at: %s" % timestamp)
        run.auto_load_balance(db=self.db)
        os.umask(env_umask)


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='clubber deamon')
    parser.add_argument('command', metavar='cmd', type=str,
                        help='deamon commands: start, stop, restart')
    return parser


def main(args):
    command = args.command
    conf = config.Config()
    logging_dir = conf.get_property('clubber', 'logging.dir')
    pidfile_suffix = ''
    pidfile = os.path.join(logging_dir, 'daemon%s.pid' % pidfile_suffix)

    if command == 'stop' or command == 'restart':
        Daemon(pidfile, None).stop()
    if command == 'start' or command == 'restart':
        database = mysql.MySQLHandler(conf.get_property('clubber', 'database'))
        d = Daemon(os.path.join(logging_dir, 'daemon%s.pid' % pidfile_suffix), database)
        d.db = database
        d.start()
    elif command == "interactive":
        database = mysql.MySQLHandler(conf.get_property('clubber', 'database'))
        run.auto_load_balance(db=database)
    elif command != 'stop':
        print(f'Usage: start | stop | restart ; unknown command: {command}')
        sys.exit(1)


if __name__ == "__main__":
    _args = get_argument_parser().parse_args()
    main(_args)
