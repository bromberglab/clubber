import argparse
from bblsuite.helper import mysql
from bblsuite.hpc import clubber
from bblsuite.hpc.submitter import Submitter


"""Webservice backend for clubber"""

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'

actions_list = [
    'create_project',
    'delete_project'
]


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Backend controller for mi-faser webinterface')

    parser.add_argument('--action', '-a', metavar='action', required=True, type=str,
                        help='actions: ' + ' | '.join(actions_list))

    parser.add_argument('--proj_id', metavar='project_id', required=False, type=int,
                        help='project id')

    parser.add_argument('--projob_id', metavar='project_job_id', required=False, type=int,
                        help='project job id')

    parser.add_argument('--job_id', metavar='job_id', required=False, type=int,
                        help='job id')

    parser.add_argument('--cluster_alias', metavar='cluster_alias', required=False, type=str,
                        help='cluster alias')

    parser.add_argument('--cluster_actions', metavar='cluster_actions', required=False, type=str,
                        help='cluster actions')

    return parser


def create_project():
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.create_project(project_id=_project_id)
    db.disconnect()


def delete_project():
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.delete_project(project_id=_project_id, local_delete=True, remote_delete=True, db_delete=True)
    db.disconnect()


def create_project_job():
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.create_project_job(project_id=_project_id, project_job_id=_project_job_id)
    db.disconnect()


def delete_project_job():
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.delete_project_job(project_id=_project_id, project_job_id=_project_job_id,
                                 local_delete=True, remote_delete=True, db_delete=True)
    db.disconnect()


def restart_job():
    db = mysql.MySQLHandler('default-clubber')
    db.connect()
    submitter = Submitter(db=db)
    submitter.restart_job(project_id=_project_id, project_job_id=_project_job_id, job_db_id=_job_id,
                          local_delete=True, remote_delete=False)
    db.disconnect()


def cluster_info():
    print(clubber.cluster_info(_cluster_alias, _cluster_actions, json_export=True))


def main():
    """main method"""

    if _action == 'create_project':
        create_project()
    elif _action == 'delete_project':
        delete_project()
    elif _action == 'create_project_job':
        create_project_job()
    elif _action == 'delete_project_job':
        delete_project_job()
    elif _action == 'restart_job':
        restart_job()
    elif _action == 'cluster_info':
        cluster_info()


if __name__ == "__main__":
    ArgParser = get_argument_parser()
    args = ArgParser.parse_args()

    _action = args.action
    _project_id = args.proj_id
    _project_job_id = args.projob_id
    _job_id = args.job_id
    _cluster_alias = args.cluster_alias
    _cluster_actions = args.cluster_actions

    main()
