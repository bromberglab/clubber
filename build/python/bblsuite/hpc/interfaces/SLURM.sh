#!/bin/bash

#PARAMS

project=${PWD##*/}
params=("$@")
file=${params[0]}
project_job=${params[1]}
transfer_dir=${params[2]}
tasks=${params[${#params[@]}-1]}
params_remaining="${params[@]:3:$#-4}"

wd_exec=`pwd`
wd=
inputdir=
script=
sources=

job_id=$SLURM_ARRAY_JOB_ID

array_id="${SLURM_ARRAY_TASK_ID:-1}"

IFS=',' read -r -a task_array <<< "$tasks"
subject="${task_array[$array_id-1]}"

if [[ -z "${SLURM_ARRAY_TASK_ID}" ]]; then
  job_id=$SLURM_JOB_ID
  wd=$wd/$job_id
  job_id_uniq=$job_id
else
  wd=$wd/$job_id.$array_id
  job_id_uniq=$job_id.$array_id
fi

transferd=$wd/$transfer_dir
job_info=$wd/job.info

#echo "prj  : $project"
#echo "slurm: $SLURM_ARRAY_TASK_ID"
#echo "aid  : $array_id"
#echo "job  : $job_id"
#echo "sub  : $subject"

export CLUBBER_WD="$wd"
export CLUBBER_INPUT_DIR="$inputdir/$project_job"
export CLUBBER_INPUT_FILE="$file"
export CLUBBER_TRANSFER_DIR="$transferd"
export CLUBBER_SOURCES_DIR="$sources"

mkdir $wd
cd $wd
mkdir $transferd
touch $job_info

echo "[${job_id_uniq}]" > $job_info
date "+start: %s" >> $job_info
$script $subject $params_remaining
date "+end: %s" >> $job_info

mv ${wd_exec}/slurm-${job_id}_${array_id}.out $transferd/clubber.out
