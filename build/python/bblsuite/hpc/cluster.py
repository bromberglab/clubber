import os.path
import datetime
import re
import time
from enum import Enum
from collections import OrderedDict
from bblsuite.helper import globals, logger, config, ssh
from bblsuite.hpc.states import DaemonState, JobState
from bblsuite.hpc.interfaces import jetstream

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Cluster:
    """Generic class representing a cluster with individual properties.

    A cluster name has to match one of the names defined in the local ssh config file ./ssh/config"""

    log = logger.Logger.get_logger()

    def __init__(self, name=None):
        self.config = config.Config()
        self.name = name
        self.inited = False
        if self.name is None:
            enabled_clusters = self.get_enabled_clusters_from_config()
            if len(enabled_clusters) != 0:
                self.name = enabled_clusters[0]
            else:
                self.log.warn('No enabled clusters available. Abort.')
                return
        else:
            if not self.config.has_property('cluster', '%s.manager' % self.name, fallback=True):
                self.log.warn('No clusters config available for alias %s' % self.name)
                return
        self.inited = True
        self.ssh = ssh.SSH(name)
        self.username = self.ssh.get_username()
        self.basedir = self.config.get_property('cluster', '%s.basedir' % self.name)
        self.queuelimit = int(self.config.get_property('cluster', '%s.queuelimit' % self.name))
        self.load_manager_type = Manager.fromstring(self.config.get_property('cluster', '%s.manager' % self.name))
        if self.load_manager_type == Manager.SGE:
            self.load_manager = SGEManager()
        elif self.load_manager_type == Manager.SLURM:
            self.load_manager = SLURMManager()
        elif self.load_manager_type == Manager.JETSTREAM:
            self.load_manager = jetstream.JETSTREAMManager()

    def connect(self):
        """Connect to cluster via ssh"""

        self.ssh.connect(open_channel=True)

    def disconnect(self):
        """Disconnect ssh connection"""

        self.ssh.disconnect()

    def is_connected(self):
        """Get connection status of cluster"""
        return self.ssh.is_alive()

    @staticmethod
    def get_available_clusters_from_config():
        conf = config.Config()
        clusters = []
        hpc_options = conf.get_section('cluster')
        for key in hpc_options:
            if key.endswith('.manager'):
                clusters += [Cluster(key[:-8])]
        return clusters

    @staticmethod
    def get_enabled_clusters_from_config():
        conf = config.Config()
        clusters = []
        enabled_clusters_name = conf.get_property('clubber', 'enabled').lower().split(',')
        hpc_options = conf.get_section('cluster')
        for key in hpc_options:
            if key.endswith('.manager'):
                if key[:-8] in enabled_clusters_name:
                    clusters += [Cluster(key[:-8])]
        return clusters

    def submit_array_job(self, jobs):
        """Submit array job"""

        start = 1
        end = jobs.__len__()
        timestamp = datetime.datetime. fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        job_template = jobs[0]
        job_template.basedir = self.basedir
        job_cmd_array_base = self.load_manager.parse_submit_command(job=job_template, array_base=[start, end])
        array_id_list = []
        for job in jobs:
            job.submit = timestamp
            array_id_list += [self.load_manager.parse_submit_command(job=job)]
        self.log.debug(
            f' + {self.name:10} > parsed array job command: {job_cmd_array_base} '
            f'[... {array_id_list.__len__()} array_ids ...]'
        )
        job_cmd_array = '%s %s' % (job_cmd_array_base, ','.join(array_id_list))
        # full cmd:
        self.log.debug(job_cmd_array)
        submit_out = self.ssh.send_shell(job_cmd_array)
        array_job_id = self.load_manager.parse_submit_output(submit_out)
        if array_job_id == -1:
            self.log.warn('Array-Job submission failed: %s' % submit_out)
        else:
            for idx, job in enumerate(jobs):
                job.basedir = self.basedir
                job.job_id = array_job_id
                job.job_array_idx = idx+1
                job.daemon = DaemonState.SUBMITTED.value
                job.status = JobState.QUEUED.value
                job.cluster = self.name

    def submit_job(self, job):
        """Submit single job to a specific cluster"""

        self.submit_jobs([job])

    def submit_jobs(self, jobs):
        """Submit multiple jobs to a specific cluster"""

        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        for job in jobs:
            job.submit = timestamp
            job.basedir = self.basedir
            job_cmd_parsed = self.load_manager.parse_submit_command(job)
            self.log.debug('parsed job command: %s' % job_cmd_parsed)
            submit_out = self.ssh.send_shell(job_cmd_parsed)
            job.job_id = self.load_manager.parse_submit_output(submit_out)
            if job.job_id == -1:
                self.log.warn('Job submission failed: %s' % submit_out)
            else:
                job.daemon = DaemonState.SUBMITTED.value
                job.status = JobState.QUEUED.value
                job.cluster = self.name

    def delete_job(self, job):
        """Delete single job from a specific cluster"""

        self.delete_jobs([job])

    def delete_jobs(self, jobs):
        """Delete multiple jobs from a specific cluster"""

        # TODO implement

    def get_all_jobs(self):
        """Get list of jobs from user active at this cluster"""

        cmd = self.load_manager.get_cmd_jobs_of_user(user=self.username)
        queue_str = self.ssh.send_shell(cmd)
        jobs = self.load_manager.parse_queue(queue_str)
        return jobs

    def get_workload(self):
        """Retrieves current cluster information"""

        cmd = self.load_manager.get_cmd_cluster_info(user=self.username)
        info_str = self.ssh.send_shell(cmd)
        return self.load_manager.parse_info(raw=info_str)

    def is_conntectable(self):
        # noinspection PyBroadException
        try:
            self.connect()
            self.disconnect()
            return 'success'
        except Exception:
            return 'error'

    def get_state_counts(self):
        """Get state counts of jobs from user active at this cluster"""

        jobs = self.get_all_jobs()
        pending = 0
        running = 0
        suspended = 0
        other = []
        for job in jobs:
            if job.status == self.load_manager.pending:
                pending += self.load_manager.get_pending_count(job)
            elif job.status == self.load_manager.running:
                running += 1
            elif job.status == self.load_manager.suspended:
                suspended += 1
            else:
                other += [job.status]
        total = pending + running + suspended + len(other)
        Cluster.log.info(
            f' + {self.name:10} > {total:4d} jobs -> '
            f'{pending:d} pending | {running:d} running | {suspended:d} suspended | {len(other):d} other'
        )
        return{'total': total, 'pending': pending, 'running': running, 'suspended': suspended, 'other': other}

    def get_disk_usage(self):
        """Get disk usage from cluster for cluster basedir"""

        cmd = 'df -H %s' % self.basedir
        output = self.ssh.send_shell(cmd).splitlines()
        header = output[0].split(maxsplit=5)
        stats = output[1].split(maxsplit=5)
        usage = {}
        for idx, name in enumerate(header):
            usage[name] = stats[idx]
        return usage

    def get_shell(self):
        """Get current shell"""

        cmd = 'echo $0'
        return self.ssh.send_shell(cmd)

    def retrieve_array_job_results(self, project, array_jobid, local_dest_path):
        """Retrieve the results from a finished array job"""

        transfer_dir = ''
        if project.remote_result_transfer_dir:
            transfer_dir = "/%s" % project.remote_result_transfer_dir

        self.ssh.TIMEOUT_EXECUTE = 30 * 60
        # res = self.ssh.get_with_find(
        #     f'find -maxdepth 2 -type d -regex ".*/{array_jobid:d}\({transfer_dir}$\|\.[0-9]+{transfer_dir}$\)"',
        #     project.get_absolute_outdir(self), local_dest_path, project.get_absolute_outdir(self))

        array_idx_min = 1
        array_idx_max = globals.RUN_VARS['CLUBBER_ARRAY_SIZE'] if 'CLUBBER_ARRAY_SIZE' in globals.RUN_VARS.keys() \
            else int(self.config.get_property('clubber', 'arrayjob.size'))
        res = self.ssh.get_with_find('$(ls -d -- %i.{%i..%i}%s/ 2>/dev/null)'
                                     % (array_jobid, array_idx_min, array_idx_max, transfer_dir),
                                     project.get_absolute_outdir(self), local_dest_path,
                                     project.get_absolute_outdir(self), delete_compressed=(not project.dbstore)
                                     )
        return res

    def retrieve_array_job_info_files(self, project, array_jobid):
        """ Retrieve array job info files """

        job_info_file = self.config.get_property('clubber', 'default.job.info')
        array_idx_min = 1
        array_idx_max = globals.RUN_VARS['CLUBBER_ARRAY_SIZE'] if 'CLUBBER_ARRAY_SIZE' in globals.RUN_VARS.keys() \
            else int(self.config.get_property('clubber', 'arrayjob.size'))
        res = self.ssh.send_shell('cat %s/%i.{%i..%i}/%s 2>/dev/null'
                                  % (project.get_absolute_outdir(self), array_jobid,
                                     array_idx_min, array_idx_max, job_info_file)
                                  )
        return res

    def retrieve_job_results(self, project, jobid, local_dest_path):
        """Retrieve the results from a finished job"""

        self.ssh.TIMEOUT_EXECUTE = 30 * 60
        res = self.ssh.get_folder(os.path.join(project.get_absolute_outdir(self), str(jobid)), local_dest_path,
                                  project.get_absolute_outdir(self), delete_compressed=(not project.dbstore))
        return res

    def retrieve_job_info_files(self, project, jobid):
        """ Retrieve job info file """

        job_info_file = self.config.get_property('clubber', 'default.job.info')
        res = self.ssh.send_shell(
            'cat %s/%i/%s 2>/dev/null' % (project.get_absolute_outdir(self), jobid, job_info_file)
        )
        return res

    def delete_remote_array_job_results(self, project, array_jobid):
        """Delete the remote results from a finished array job"""

        remote_array_dir = os.path.join(project.get_absolute_outdir(self), str(array_jobid))
        if remote_array_dir == '':
            self.log.warn('Error while deleting remote array job results: remote_array_dir is empty')
        else:
            self.log.debug('Removing remote array job results: %s' % remote_array_dir)
            array_idx_min = 1
            array_idx_max = globals.RUN_VARS['CLUBBER_ARRAY_SIZE'] if 'CLUBBER_ARRAY_SIZE' in globals.RUN_VARS.keys() \
                else int(self.config.get_property('clubber', 'arrayjob.size'))
            self.ssh.remove_dir("%s.{%i..%i}" % (remote_array_dir, array_idx_min, array_idx_max))

    def delete_remote_job_results(self, project, jobid):
        """Delete the remote results from a finished job"""

        remote_jobdir = os.path.join(project.get_absolute_outdir(self), str(jobid))
        if remote_jobdir == '':
            self.log.warn('Error while deleting remote job results: remote_jobdir is empty')
        else:
            self.log.debug('Removing remote job results: %s' % remote_jobdir)
            self.ssh.remove_dir(remote_jobdir)

    def delete_remote_project_input(self, project, project_job):
        """Delete the remote results from a finished array job"""

        remote_project_inputdir = os.path.join(project.get_absolute_input_dir(self), str(project_job))
        if remote_project_inputdir == '':
            self.log.warn('Error while deleting remote job input directory: remote_project_inputdir is empty')
        else:
            self.log.debug('Removing remote project input directory: %s' % remote_project_inputdir)
            self.ssh.remove_dir(remote_project_inputdir)


class Project:
    """Generic project: contains jobs and to run on a (set of) cluster(s)"""

    log = logger.Logger.get_logger()

    def __init__(self, name, subid=None, projectid=None, valid_clusters=None,
                 remote_result_transfer_dir=None, script=None, dbstore=False):
        self.conf = config.Config()
        self.name = name
        self.subid = subid
        self.id = projectid
        self.description = None
        self.user = None
        self.submit_script = self.conf.get_property('clubber', 'default.remote.wrapper')
        self.remote_output_dir = self.conf.get_property('clubber', 'remote.output.dir')
        self.remote_input_dir = self.conf.get_property('clubber', 'remote.input.dir')
        self.local_working_dir = self.conf.get_property('clubber', 'local.working.dir')
        self.local_input_dir = self.conf.get_property('clubber', 'local.input.dir')
        self.local_output_dir = self.conf.get_property('clubber', 'local.output.dir')
        if remote_result_transfer_dir is None:
            self.transfer = False
            self.remote_result_transfer_dir = self.conf.get_property('clubber', 'remote.transfer.dir')
        else:
            self.transfer = True
            self.remote_result_transfer_dir = remote_result_transfer_dir
        if script:
            self.script = script
        else:
            self.script = self.conf.get_property('clubber', 'default.remote.script')
        self.clusters = valid_clusters
        self.sources = None
        self.pre_processor = None
        self.post_processor = None
        self.dbstore = dbstore
        self.arrayjob_size = None

    def duplicate(self):
        p = Project(
            self.name, self.subid, self.id, self.clusters, self.remote_result_transfer_dir, self.script, self.dbstore
        )
        p.conf = config.Config()
        p.description = self.description
        p.user = self.user
        p.submit_script = self.submit_script
        p.remote_output_dir = self.remote_output_dir
        p.remote_input_dir = self.remote_input_dir
        p.local_working_dir = self.local_working_dir
        p.local_input_dir = self.local_input_dir
        p.local_output_dir = self.local_output_dir
        p.transfer = self.transfer
        p.sources = self.sources
        p.pre_processor = self.pre_processor
        p.post_processor = self.post_processor
        p.arrayjob_size = self.arrayjob_size
        return p

    def set_description(self, text):
        self.description = text

    @staticmethod
    def create_registered(project_id, project_name, project_clusters, project_transfer_dir,
                          project_script, project_dbstore):
        project = Project(project_name, projectid=project_id, valid_clusters=project_clusters,
                          remote_result_transfer_dir=project_transfer_dir,
                          script=project_script, dbstore=project_dbstore)
        return project

    @staticmethod
    def create_registered_full(project_id, project_name, project_description, project_script, project_sources,
                               project_clusters, project_transfer_dir, project_pre_processor, project_post_processor,
                               project_dbstore):
        project = Project(project_name, projectid=project_id, valid_clusters=project_clusters,
                          remote_result_transfer_dir=project_transfer_dir,
                          script=project_script, dbstore=project_dbstore)
        project.description = project_description
        project.sources = project_sources
        project.pre_processor = project_pre_processor
        project.post_processor = project_post_processor
        return project

    def get_absolute_outdir(self, cluster):
        return os.path.join(cluster.basedir, '%s%s%s' % (self.name, os.path.sep, self.remote_output_dir))

    def get_absolute_input_dir(self, cluster):
        return os.path.join(cluster.basedir, '%s%s%s' % (self.name, os.path.sep, self.remote_input_dir))

    def get_local_input_dir(self, project_subid):
        return os.path.join(
                    os.path.join(
                        os.path.join(
                            self.local_working_dir, self.name
                        ), str(project_subid)
                    ), self.local_input_dir
        )

    def get_local_output_dir(self, project_subid):
        return os.path.join(
                    os.path.join(
                        os.path.join(
                            self.local_working_dir, self.name
                        ), str(project_subid)
                    ), self.local_output_dir
        )


class Project_Job:
    """Generic project: contains project jobs (submissions) to run on a (set of) cluster(s)"""

    def __init__(self, projectjobid, project_id, status, progress, jobfile, args):
        self.id = projectjobid
        self.project_id = project_id
        self.status = status
        self.progess = progress
        self.jobfile = jobfile
        self.args = args


class Job:
    """Generic grid job"""

    log = logger.Logger.get_logger()

    def __init__(self, project=None, cmd=None):

        if project is not None:
            self.project = project
            self.name = self.project.name
        else:
            self.project = None
            self.name = None

        self.basedir = None  # cluster config

        self.db_id = None  # db
        self.created = None  # db
        self.updated = None  # db
        self.daemon = None
        self.cluster = None  # db
        self.cmd = cmd  # db
        self.file = None  # db
        self.result = None  # db
        self.end = None  # db
        self.restarts = 0  # db

        self.job_id = None  # queue
        self.job_array_idx = None  # queue
        self.array_info = None  # queue
        self.status = None  # queue
        self.submit = None  # queue
        self.start = None  # queue

        self.project_job = None  # manual

    @property
    def is_array_job(self):
        """ Checks if thi job is an array or single job """

        return self.job_array_idx is not None and self.job_array_idx is not 'NULL'

    def get_cluster_job_id(self):
        """ Get full job id """

        if self.is_array_job:
            return '%s%s%s' % (self.job_id, self.cluster.load_manager.jobid_seperator, self.job_array_idx)
        else:
            return self.job_id

    def get_cluster_job_output_id(self):
        """ Get full job id """

        if self.is_array_job:
            return '%s%s%s' % (self.job_id, self.cluster.load_manager.output_seperator, self.job_array_idx)
        else:
            return self.job_id

    @staticmethod
    def create_from_queue(job_id, array_idx, array_info, status, start):
        job = Job()
        job.job_id = job_id
        job.job_array_idx = array_idx
        job.array_info = array_info
        job.status = status
        job.start = start
        return job

    @staticmethod
    def create_registered(db_id, project_name, project_id, project_job_id, cmd, file, restarts):
        project = Project(project_name, projectid=project_id, subid=project_job_id)
        job = Job(project=project, cmd=cmd)
        job.db_id = db_id
        job.file = file
        job.restarts = restarts
        return job

    @staticmethod
    def create_submitted_minimal(db_id, cluster, jobid, arrayidx, status, restarts):
        job = Job(project=None, cmd=None)
        job.cluster = cluster
        job.db_id = db_id
        job.job_id = jobid
        job.job_array_idx = arrayidx
        job.status = status
        job.restarts = restarts
        return job

    @staticmethod
    def create_submitted_detailed(db_id, project_name, cmd, created, updated, cluster, jobid, arrayidx,
                                  status, submit, restarts):
        project = Project(project_name)
        job = Job(project=project, cmd=cmd)
        job.db_id = db_id
        job.created = created
        job.updated = updated
        job.cluster = cluster
        job.job_id = jobid
        job.job_array_idx = arrayidx
        job.status = status
        job.submit = submit
        job.restarts = restarts
        return job


class ClusterManager:
    """Abstract class for a cluster manager"""

    def __init__(self):
        self.__init_options()

    def __init_options(self):
        """Init SGE representations of submit options"""

        self.options = OrderedDict()
        self.__add_option(
            'job_name', '--job_name=__Name of the job__|'
                        '[ Not necessary if job is submitted within a project ]'
        )
        self.__add_option(
            'working_dir', '--working_dir=__Job working directory__|'
                           '[ Not necessary if job is submitted within a project ]'
        )
        self.__add_option(
            'stdout_file', '--stdout_file=__Path to stdout file__|'
                           '[ Optional {default: jobname.jobid.out in working_dir}; ]'
        )
        self.__add_option(
            'stderr_file', '--stderr_file=__Path to err file__|'
                           '[ Optional {default: jobname.jobid.error in working_dir}; ]'
        )
        self.__add_option(
            'script', '--script=__Path to script file__|'
                      '[ Optional {default: submit.sh / set in project}; Executable job script RELATIVE to working_dir]'
        )
        self.__add_option(
            'args', '--args="__arg1__ __arg2__ __...__"|'
                    '[ Optional {default: None}; Arguments for executed script, eg: "arg1 arg2 ..." ]'
        )
        self.__add_option(
            'binary', '--binary|'
                      '[ Optional; Set switch if a binary should be executed rather than a script ]'
        )
        self.__add_option(
            'array', '--array|'
                     '[ Optional; Set switch if job is part of an array-job: "--args" must be set! ]'
        )

    def __add_option(self, key, val):
        """Add SGE representation of submit option"""

        self.options[key] = val

    def get_option(self, key):
        """Get SGE representation of submit option"""

        return self.options[key]


class SGEManager:
    """Manager implementation to execute submissions on SGE based hpc systems"""

    running = 'r'
    pending = 'qw'
    suspended = 's'

    output_seperator = "."
    jobid_seperator = "."

    def __init__(self):
        self.cmd_queue = 'qstat'
        self.cmd_submit = 'qsub'
        self.cmd_delete = 'qdel'
        self.cmd_accounting = 'qacct'
        self.cmd_info = 'qhost'
        self.__init_options()
        self.input_pattern = "(?:[^\s\"']+\"[^\"]*\"|[^\s\"]+)"

    def __init_options(self):
        """Init SGE representations of submit options"""

        self.options = {}
        self.__add_option('job_name', '-N ')
        self.__add_option('working_dir', '-wd ')
        self.__add_option('stdout_file', '-o ')
        self.__add_option('stderr_file', '-e ')
        self.__add_option('binary', '-b ')
        self.__add_option('script', None)
        self.__add_option('args', None)
        self.__add_option('array', '-t ')

    def __add_option(self, key, val):
        """Add SGE representation of submit option"""

        self.options[key] = val

    def get_option(self, key):
        """Get SGE representation of submit option"""

        return self.options[key]

    def parse_delete_command(self):
        """Mark job for deletion"""

        # TODO implement parse_delete_command
        pass

    def parse_submit_command(self, job, extension=None, array_base=None):
        """Return the correct submission format for SGE representing given options

        <extension> is added to the command as is"""

        command_parsed = '%s' % self.cmd_submit
        if array_base is not None:
            command_parsed += ' %s%i-%i' % (self.get_option('array'), array_base[0], array_base[1])
        regmatches = re.findall(self.input_pattern, job.cmd)

        # set default args as empty as those are optional
        args = {'args': ''}

        # add required parameters
        job_required_args = '"%s" "%i" "%s"' % (job.file, job.project.subid, job.project.remote_result_transfer_dir)

        # set default parameters from project, may be overwritten by job commands
        if job.project is not None:
            args['working_dir'] = job.project.name
            args['job_name'] = job.project.name
            args['script'] = job.project.script

        for match in regmatches:
            if '=' in match:
                key, val = match.split('=', 1)
            else:
                key = match
                val = None
            if key.startswith('--'):
                key = key[2:]
            args[key] = val
        array = False
        binary = False
        if 'binary' in args.keys():
            if args['binary'] == "y":
                binary = True
        for key, val in args.items():
            if key == 'working_dir':
                command_parsed += ' %s%s/%s' % (self.get_option(key), job.basedir, val)
            elif key == 'args':
                # remove quotes
                if val.startswith("\""):
                    val = val[1:]
                if val.endswith("\""):
                    val = val[:-1]
                args[key] = val
            elif key == 'script':
                # remove quotes
                if val.startswith("\""):
                    val = val[1:]
                if val.endswith("\""):
                    val = val[:-1]
                test_args = val.split(' ', 1)
                if len(test_args) != 1:
                    val = test_args[0]
                    args['args'] = test_args[1]
                # add nothing if binary
                if binary:
                    args[key] = val
                # add project wd if not binary
                else:
                    args[key] = '%s/%s/%s' % (job.basedir, args['working_dir'], val)
            elif key == 'array':
                if array_base is None:
                    array = True
                    if val is not None:
                        args['args'] = val
            else:
                command_parsed += ' %s%s' % (self.get_option(key), val)
        if extension is not None:
            command_parsed += f' {extension}'
        command_parsed += f' {job.basedir}/{args["working_dir"]}/{job.project.submit_script} {job_required_args}'
        # TODO check args['script'] here?
        if not array and args['args']:
            command_parsed += ' %s' % args['args']
        elif array and array_base is None:
            _args = args['args']
            if _args.startswith("\""):
                _args = _args[1:]
            if _args.endswith("\""):
                _args = _args[:-1]
            command_parsed = '"%s"' % _args
        return command_parsed

    def get_cmd_jobs_of_user(self, user="*", filter_flags=None):
        """Get jobs of all users or all users if no user specified; optional filter by status"""

        filter_str = '-s psr'
        if filter_flags is not None:
            filter_str = '-s %s' % filter_flags
        cmd = '%s -u %s %s' % (self.cmd_queue, user, filter_str)
        return cmd

    def get_cmd_cluster_info(self, user=None):
        """Use sinfo to get cluster info"""

        format_str = "-q"
        param_str = "-u %s" % user
        cmd = '%s %s %s' % (self.cmd_info, param_str, format_str)
        return cmd

    @staticmethod
    def get_pending_count(job):
        """Get pending count"""

        return SGEManager.get_pending_info(job)[0]

    @staticmethod
    def get_pending_info(job):
        """Get pending state, check for pending array jobs

        4052349 0.55500 fusion2bl  mmiller      qw    12/27/2016 07:51:31        1 952-1000:1"""

        pattern = r'([0-9]+)-([0-9]+):[0-1]'
        matched = re.match(pattern, job.array_info)
        if matched is not None:
            array_start = int(matched.group(1))
            array_end = int(matched.group(2))
            array_queued = array_end - (array_start - 1)
            return array_queued, (array_start, array_end)
        return 1, None

    def parse_queue(self, queue_out):
        """Parse queue output:

        job-ID  prior   name       user         state submit/start at     queue                        slots ja-task-ID
        ---------------------------------------------------------------------------------------------------------------
        3503210 0.55500 fusn2      mmiller      r     11/30/2016 13:40:17 pow12.q@c9n4.centauri            1

        """

        jobs = []
        queue_list = queue_out.split('\n')[2:]
        for entry in queue_list:
            job_details = entry.split()

            try:
                array_idx = int(job_details[-1])
                int(job_details[-2])
            except ValueError:
                array_idx = None
            except IndexError:
                array_idx = None
                Cluster.log.warn(f'Could not parse job from queue: {entry} [{self.cmd_accounting}]')

            job = Job.create_from_queue(job_details[0], array_idx, job_details[-1], job_details[4], '%sT%s' %
                                        (job_details[5], job_details[6]))
            jobs += [job]
        return jobs

    def parse_info(self, raw):
        """Parse info output:

                HOSTNAME                ARCH         NCPU  LOAD  MEMTOT  MEMUSE  SWAPTO  SWAPUS
        -------------------------------------------------------------------------------
        global                  -               -     -       -       -       -       -
        c0n2                    lx26-amd64      8  0.00   23.5G  491.7M   24.0G     0.0
           all.q                BIP   0/0/8
           interactive.q        I     0/0/16
        c0n3                    lx26-amd64      8  0.00   23.5G  543.2M   24.0G   14.6M
           all.q                BIP   0/0/8
           infinity.q           BIP   0/1/8
           interactive.q        I     0/0/16
        c0n4                    lx26-amd64      8     -   23.5G       -   24.0G       -
           all.q                BIP   0/0/8         au
           pow12.q              BP    0/0/8         au
           pow13.q              BP    0/0/8         au
           pow15.q              BP    0/0/8         au
           pow18.q              BP    0/0/8         au
           pow19.q              BP    0/0/8         au
           pow20.q              BP    0/0/8         au
           pow22.q              BP    0/0/8         adu
           mcore.q              BP    0/0/8         au
        """

        info = {
            'nodes_summary': {'allocated': 0, 'mixed': 0, 'other': 0, 'total': 0},
            'cpus_summary': {'allocated': 0, 'idle': 0, 'other': 0, 'total': 0},
            'nodes': []
        }
        info_raw = {}
        header_mapping = []
        entries = raw.split('\n')
        header = entries[0]
        _header = re.split('\s+', header)
        _header.extend(['SLOTS(R/U/A)'])
        for key in _header:
            header_mapping.append(key)
            info_raw[key] = []
        for entry in entries[2:]:
            if entry.startswith('global'):
                continue
            elif not entry.startswith(' '):
                val_list = re.split('\s+', entry)
                pos = 0
                for v in val_list:
                    info_raw[header_mapping[pos]].append(v)
                    pos += 1
                info_raw[header_mapping[pos]].append({'reserved': 0, 'used': 0, 'available': 0})
            else:
                current_idx = len(info_raw[header_mapping[0]])-1
                queue_stats = re.split('\s+', entry)[1:]
                queue_name = queue_stats[0]
                queue_types = queue_stats[1]
                queue_slots = queue_stats[2].split("/")
                queue_state = None
                interactive_only = False
                inactive_slots = False
                if len(queue_stats) > 3 and len(queue_stats[3].strip()) > 0:
                    queue_state = queue_stats[3].strip()
                if 'B' not in queue_types:
                    interactive_only = True
                if queue_state:
                    inactive_slots = True
                node_slots = info_raw['SLOTS(R/U/A)'][current_idx]
                node_cpus = int(info_raw['NCPU'][current_idx])
                slots_reserved = int(queue_slots[0])
                slots_used = int(queue_slots[1])
                slots_available = int(queue_slots[2])
                node_slots['reserved'] = min(node_cpus, node_slots['reserved'] + slots_reserved)
                node_slots['used'] = min(node_cpus, node_slots['used'] + slots_used)
                node_slots['available'] = node_cpus
                Cluster.log.debug(f'queue: {queue_name}, interactive only: {interactive_only}, '
                                  f'inactive slots: {inactive_slots}, slots_available: {slots_available:d}'
                                  )

        nodes = set()
        for idx, node in enumerate(info_raw['HOSTNAME']):
            if node in nodes:
                continue
            cpu_states = info_raw['SLOTS(R/U/A)'][idx]
            allocated = cpu_states['reserved'] + cpu_states['used']
            idle = cpu_states['available'] - (cpu_states['reserved'] + cpu_states['used'])
            other = 0
            total = cpu_states['available']
            info['cpus_summary']['allocated'] += allocated
            info['cpus_summary']['idle'] += idle
            info['cpus_summary']['other'] += other
            info['cpus_summary']['total'] += total
            if info['cpus_summary']['idle'] == 0:
                info['nodes_summary']['allocated'] += 1
            else:
                info['nodes_summary']['mixed'] += 1
            info['nodes_summary']['total'] += 1
            info['nodes'].append([node, allocated, idle, other, total])
            nodes.add(node)
        Cluster.log.debug(f'Checked cluster [{self.cmd_info}]')
        return info

    @staticmethod
    def parse_submit_output(output):
        """Parsing SGE output of submit call:

        Your job 3900903 ("jobname") has been submitted or
        Your job-array 4052289.1-6:1 ("jobname") has been submitted"""

        # prefix = output[0:0]
        job_id = output.split(' ')[2]
        job_id = job_id.split('.')[0]

        try:
            job_id = int(job_id)
        except ValueError:
            job_id = -1

        return job_id


class SLURMManager:
    """Manager implementation to execute submissions on SLURM based hpc systems"""

    running = 'R'
    pending = 'PD'
    suspended = 'S'

    output_seperator = "."
    jobid_seperator = "_"

    def __init__(self):
        self.cmd_queue = 'squeue'
        self.cmd_submit = 'sbatch'
        self.cmd_delete = 'scancel'
        self.cmd_accounting = 'sacct'
        self.cmd_info = 'sinfo'
        self.__init_options()
        self.input_pattern = "(?:[^\s\"']+\"[^\"]*\"|[^\s\"]+)"

    def __init_options(self):
        """Init SLURM representations of submit options"""

        self.options = {}
        self.__add_option('job_name', '--job-name=')
        self.__add_option('working_dir', '--workdir=')
        self.__add_option('stdout_file', '--output=')
        self.__add_option('stderr_file', '--error=')
        self.__add_option('binary', '--wrap=')
        self.__add_option('script', None)
        self.__add_option('args', None)
        self.__add_option('array', '--array=')

    def __add_option(self, key, val):
        """Add SLURM representation of submit option"""

        self.options[key] = val

    def get_option(self, key):
        """Get SLURM representation of submit option"""

        return self.options[key]

    def parse_delete_command(self):
        """Mark job for deletion"""

        # TODO implement parse_delete_command
        pass

    def parse_submit_command(self, job, extension=None, array_base=None):
        """Return the correct submission format for SLURM representing given options

        <extension> is added to the command as is"""

        command_parsed = '%s' % self.cmd_submit
        if array_base is not None:
            command_parsed += ' %s%i-%i' % (self.get_option('array'), array_base[0], array_base[1])
        regmatches = re.findall(self.input_pattern, job.cmd)

        # set default args as empty as those are optional
        args = {'args': ''}

        # add required parameters
        job_required_args = '"%s" "%i" "%s"' % (job.file, job.project.subid, job.project.remote_result_transfer_dir)

        # set default parameters from project, may be overwritten by job commands
        if job.project is not None:
            args['working_dir'] = job.project.name
            args['job_name'] = job.project.name
            args['script'] = job.project.script

        for match in regmatches:
            if '=' in match:
                key, val = match.split('=', 1)
            else:
                key = match
                val = None
            if key.startswith('--'):
                key = key[2:]
            args[key] = val
        array = False
        binary = False
        if 'binary' in args.keys():
            if args['binary'] == "y":
                binary = True
        for key, val in args.items():
            if key == 'working_dir':
                command_parsed += ' %s%s/%s' % (self.get_option(key), job.basedir, val)
            elif key == 'args':
                # remove quotes
                if val.startswith("\""):
                    val = val[1:]
                if val.endswith("\""):
                    val = val[:-1]
                args[key] = val
            elif key == 'script':
                # remove quotes
                if val.startswith("\""):
                    val = val[1:]
                if val.endswith("\""):
                    val = val[:-1]
                test_args = val.split(' ', 1)
                if len(test_args) != 1:
                    val = test_args[0]
                    args['args'] = test_args[1]
                # add nothing if binary
                if binary:
                    args[key] = val
                # add project wd if not binary
                else:
                    args[key] = '%s/%s/%s' % (job.basedir, args['working_dir'], val)
            elif key == 'array':
                if array_base is None:
                    array = True
                    if val is not None:
                        args['args'] = val
            else:
                command_parsed += ' %s%s' % (self.get_option(key), val)
        if extension is not None:
            command_parsed += ' %s' % extension
        if binary:
            command_parsed += ' %s"%s"' % (self.get_option('binary'), args['script'])
        else:
            command_parsed += f' {job.basedir}/{args["working_dir"]}/{job.project.submit_script} {job_required_args}'
            # TODO check args['script'] here?
        if not array and args['args']:
            command_parsed += ' %s' % args['args']
        elif array and array_base is None:
            _args = args['args']
            if _args.startswith("\""):
                _args = _args[1:]
            if _args.endswith("\""):
                _args = _args[:-1]
            command_parsed = '"%s"' % _args
        return command_parsed

    def get_cmd_jobs_of_user(self, user=None, filter_flags=None):
        """Get jobs of all users or all users if no user specified; optional filter by status"""

        format_str = '--local -o %i,%t,%S'
        filter_str = '-t pd,r,s,cg'
        param_str = ''  # ''--clusters=all'
        if user is None:
            user_filter = ''
        else:
            user_filter = '-u %s' % user
        if filter_flags is not None:
            filter_str = '-t %s' % filter_flags
        cmd = '%s %s %s %s %s' % (self.cmd_queue, format_str, user_filter, param_str, filter_str)
        return cmd

    def get_cmd_cluster_info(self, user=None):
        """Use sinfo to get cluster info"""

        # TODO check user here?
        str(user)
        format_str = "%N %P %.5a %C"
        param_str = "-N"
        cmd = '%s %s -o "%s"' % (self.cmd_info, param_str, format_str)
        return cmd

    @staticmethod
    def get_pending_count(job):
        """Get pending count"""

        return SLURMManager.get_pending_info(job)[0]

    @staticmethod
    def get_pending_info(job):
        """Get pending state, check for pending array jobs

        1404367_[413-1000]      main fusion2b   cmm591 PD       0:00      1 (Resources)"""

        # pattern = r'([0-9]+)_\[([0-9]+)-([0-9]+)\]'
        # matched = re.match(pattern, job.job_id)
        pattern = r'\[([0-9]+)-([0-9]+)\]'
        matched = re.match(pattern, job.array_info)
        if matched is not None:
            array_start = int(matched.group(1))
            array_end = int(matched.group(2))
            array_queued = array_end - (array_start - 1)
            return array_queued, (array_start, array_end)
        return 1, None

    def parse_queue(self, queue_out):
        """Parse queue output:

        standard:
            JOBID  PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
           1395100   testing canu_eco    ak917 PD       0:00      1 (Resources)

        formatted:
            JOBID,ST,START_TIME
            1396062,CD,2016-12-25T04:33:33
        """

        jobs = []
        queue_list = queue_out.split('\n')
        for entry in queue_list:
            if entry == '' or entry.startswith('slurm_') or entry.startswith('CLUSTER') or entry.startswith('JOBID'):
                continue

            try:
                int(entry[0])
            except (ValueError, IndexError):
                Cluster.log.warn('Could not parse job from queue: %s' % entry)

            job_details = entry.split(',')

            job_id_detail = job_details[0].split(self.jobid_seperator)
            if len(job_id_detail) == 1:
                job_id_detail.append("")
                array_idx = None
            else:
                try:
                    array_idx = int(job_id_detail[1])
                except (ValueError, IndexError):
                    array_idx = None

            job = Job.create_from_queue(job_id_detail[0], array_idx, job_id_detail[1], job_details[1], job_details[2])
            jobs += [job]
        return jobs

    def parse_info(self, raw):
        """Parse info output:

        PARTITION AVAIL NODES CPUS(A/I/O/T)
        main    up  1 24/0/0/24

        """

        info = {
            'nodes_summary': {'allocated': 0, 'mixed': 0, 'other': 0, 'total': 0},
            'cpus_summary': {'allocated': 0, 'idle': 0, 'other': 0, 'total': 0},
            'nodes': []
        }
        info_raw = {}
        header_mapping = []
        entries = raw.split('\n')
        header = entries[0]
        for key in re.split('\s+', header):
            header_mapping.append(key)
            info_raw[key] = []
        for entry in entries[1:]:
            val_list = re.split('\s+', entry)
            pos = 0
            for v in val_list:
                if pos == 2 and not re.match('\s+.*[0-9]+/[0-9]+/[0-9]+.*', v):
                    pos += 1
                    continue
                info_raw[header_mapping[pos]].append(v)
                pos += 1

        nodes = set()
        for idx, node in enumerate(info_raw['NODELIST']):
            if node in nodes:
                continue
            cpu_states = info_raw['CPUS(A/I/O/T)'][idx].split("/")
            allocated = int(cpu_states[0])
            idle = int(cpu_states[1])
            other = int(cpu_states[2])
            total = int(cpu_states[3])
            info['cpus_summary']['allocated'] += allocated
            info['cpus_summary']['idle'] += idle
            info['cpus_summary']['other'] += other
            info['cpus_summary']['total'] += total
            if info['cpus_summary']['idle'] == 0:
                info['nodes_summary']['allocated'] += 1
            else:
                info['nodes_summary']['mixed'] += 1
            info['nodes_summary']['total'] += 1
            info['nodes'].append([node, allocated, idle, other, total])
            nodes.add(node)
        Cluster.log.debug(f'Checked cluster [{self.cmd_info}]')
        return info

    @staticmethod
    def parse_submit_output(output):
        """Parsing SLURM output of submit call:

        Submitted batch job 1391989"""

        # prefix = output[0:19]
        job_id = output.split(' ')[3]  # or: output[20:]

        try:
            job_id = int(job_id)
        except ValueError:
            job_id = -1

        return job_id


class Manager(Enum):
    """Enum class containing implemented cluster managers/job submission systems"""

    SGE = 1
    SLURM = 2
    JETSTREAM = 3

    @classmethod
    def fromstring(cls, name):
        return getattr(cls, name.upper(), None)
