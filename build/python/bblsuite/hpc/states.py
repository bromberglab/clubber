from enum import Enum

"""Daemon and Job states"""

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class DaemonState(Enum):
    """Enum class containing all possible daemon states"""

    REGISTERED = 0
    FETCHED = 1
    SUBMITTED = 2
    VALIDATED = 3
    TRANSFERRING = 4
    TRANSFERRED = 5
    FAILED = 6

    @classmethod
    def fromstring(cls, name):
        return getattr(cls, name.upper(), None)


class JobState(Enum):
        """Enum class containing all possible daemon states"""

        UNASSIGNED = 0
        QUEUED = 1
        RUNNING = 2
        FINISHED = 3
        FAILED = 4

        @classmethod
        def fromstring(cls, name):
            return getattr(cls, name.upper(), None)
