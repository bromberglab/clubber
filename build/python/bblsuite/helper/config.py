#!/usr/bin/env python3
import os
import copy
import configparser
from bblsuite.helper import globals

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Config:
    """Generic class to handle and parse config files"""

    config_inited = False
    config_file = None
    config_obj = None
    config_default = None

    DEFAULT_LOG_LEVEL = 'INFO'
    DEFAULT_LOG_FILE = 'bblsuite.log'
    DEFAULT_CONFIG_DIR = 'config'
    DEFAULT_CONFIG_FILE = 'main.conf'

    def __init__(self, config_file=False, force=False, default=False):
        if force and not default:
            Config.config_inited = False
        if Config.config_inited and not default:
            self.config = Config.config_obj
            return
        if default and self.config_default and not force:
            return
        if not config_file:
            self.config_file = self.DEFAULT_CONFIG_FILE
        else:
            self.config_file = config_file
        self.logfile = None
        self.loglevel = self.DEFAULT_LOG_LEVEL

        self.config = configparser.ConfigParser()
        # try to find config file recursively from current working directory
        if default:
            parsed = False
        else:
            if config_file:
                parsed = self.__parse_config_file(config_file=config_file)
            else:
                parsed = self.__parse_config_file()
        # try to find config file recursively from config module
        if not parsed:
            path = os.path.abspath(__file__)
            dir_path = os.path.dirname(path)
            parsed = self.__parse_config_file(config_dir=dir_path)

        # no config found, return
        if not parsed:
            print(f'[ config ] WARNING: No config file found: {self.DEFAULT_CONFIG_FILE}')
            return

        if not default:
            if self.config.has_section('Logging'):
                self.loglevel = self.DEFAULT_LOG_LEVEL
                self.logfile = self.DEFAULT_LOG_FILE
                if self.has_property('Logging', 'level'):
                    self.loglevel = self.get_property('Logging', 'level')
                if self.has_property('Logging', 'file'):
                    self.logfile = self.get_property('Logging', 'file')
            Config.config_file = self.config_file
            Config.config_obj = self.config
            Config.config_inited = parsed
        else:
            Config.config_default = copy.copy(self.config)

        if self.logfile and self.loglevel in ("DEBUG") and not globals.QUIET:
            if os.access(self.logfile, os.W_OK):
                print(f'[ config ] Logging to {self.logfile} with level {self.loglevel}')
            else:
                print(f'[ config ] Logging to stdout with level {self.loglevel} - no permission to log at: {self.logfile}')

    def get_config_dir(self):
        return os.path.dirname(str(self.config_file))

    def update(self):
        self.config.read(self.config_file)
        Config.config_obj = self.config

    def __parse_config_file(self, config_dir=os.getcwd(), config_file=False):
        """Parse config file. If no file is specified search recursively for file named in DEFAULT_CONFIG"""

        env_wd = os.getcwd()
        if not config_file:
            os.chdir(config_dir)
            current_wd = config_dir
            config_file = os.path.join(current_wd, self.config_file)
            conf_file_dir = '%s/%s/%s' % (current_wd, self.DEFAULT_CONFIG_DIR, self.config_file)
            while (not os.path.exists(config_file) and not os.path.exists(conf_file_dir)) and os.getcwd() != '/':
                os.chdir('../')
                config_file = '%s/%s' % (os.getcwd(), self.config_file)
                conf_file_dir = '%s/%s/%s' % (os.getcwd(), self.DEFAULT_CONFIG_DIR, self.config_file)
            if not os.path.exists(config_file) and not os.path.exists(conf_file_dir):
                os.chdir(env_wd)
                return False
            os.chdir(current_wd)
            if not os.path.exists(config_file):
                config_file = conf_file_dir
        else:
            if not os.path.exists(config_file):
                print(f'[ config ] WARNING: Config file not found: {config_file}')
                os.chdir(env_wd)
                return False

        if self.loglevel in ("DEBUG") and not globals.QUIET:
            print(f'[ config ] Read from: {config_file}')

        self.config_file = config_file
        self.config.read(config_file)
        os.chdir(env_wd)
        return True

    def get_section(self, section):
        """Get all properties within a specific section"""
        if self.is_section(section, fallback=False):
            return self.config.options(section)
        else:
            if self.is_section(section, fallback=True):
                return Config(default=True).config_default.options(section)
            else:
                raise Exception('Cound not find configuration for [%s].' % section)

    def is_section(self, section, fallback=True):
        """Check for availability of a section"""

        found_section = self.config.has_section(section)
        if not fallback or found_section:
            return found_section
        else:
            print(f'[ config ] Could not find: [{section}] in config file. Searching default configuration.')
            return Config(default=True).config_default.has_section(section)

    def get_property(self, section, option):
        """Get configuration property within a specific section"""

        if self.has_property(section, option, fallback=False):
            return self.config.get(section, option)
        else:
            if self.has_property(section, option, fallback=True):
                return Config(default=True).config_default.get(section, option)
            else:
                raise Exception('Cound not find configuration for [%s] %s.' % (section, option))

    def has_property(self, section, option, fallback=True):
        """Check for availability of a configuration property within a specific section"""

        has_option = self.config.has_option(section, option)
        if not fallback or has_option:
            return has_option
        else:
            print(f'[ config ] Could not find: [{section}] {option} in config file. Searching default configuration.')
            return Config(default=True).config_default.has_option(section, option)
