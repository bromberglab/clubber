#!/usr/bin/env python3
import os
import os.path
import random
import string
import tarfile
import paramiko
import threading
from bblsuite.helper import config, logger

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class SSH:
    """Manage SSH related actions"""

    TIMEOUT_CONNECT_DEFAULT = 60
    TIMEOUT_EXECUTE_DEFAULT = 600

    TIMEOUT_CONNECT = TIMEOUT_CONNECT_DEFAULT
    TIMEOUT_EXECUTE = TIMEOUT_EXECUTE_DEFAULT

    log = logger.Logger.get_logger()
    paramiko_logger = logger.Logger.get_logger(name="paramiko", level="WARN")

    def __init__(self, ssh_config_name, hostname=None, username=None, password=None, port=22):
        self.config = config.Config()
        self.client = None
        self.transport = None
        self.sftp = None
        self.shell = None
        self.process = None
        self.ssh_config_file = None
        self.ssh_config_name = None
        self.client_config = None
        self.thread = None
        self.cfg = {}
        self.auth_type = 'passwd'
        if not hostname:
            if self.config.has_property('SSH', '%s.hostname' % ssh_config_name):
                _port = port
                if self.config.has_property('SSH', '%s.port' % ssh_config_name):
                    _port = int(self.config.get_property('SSH', '%s.port' % ssh_config_name))
                self.cfg = {
                    'hostname': self.config.get_property('SSH', '%s.hostname' % ssh_config_name),
                    'port': _port
                }
                self.auth_type = self.config.get_property('SSH', '%s.auth' % ssh_config_name)
                self.cfg['username'] = self.config.get_property('SSH', '%s.username' % ssh_config_name)
                if self.auth_type == 'idrsa':
                    self.has_identity_file = True
                    id_file = self.config.get_property('SSH', '%s.idfile' % ssh_config_name)
                    if not id_file.startswith('/'):
                        id_file = os.path.join(os.path.dirname(self.config.config_file), id_file)
                    self.cfg['key_filename'] = id_file
                elif self.auth_type == 'passwd':
                    self.cfg['password'] = self.config.get_property('SSH', '%s.password' % ssh_config_name)
                    self.has_identity_file = False
            else:
                if self.parse_ssh_config() and self.load_alias_config(ssh_config_name):
                    if 'identityfile' in self.cfg.keys():
                        self.has_identity_file = True
                else:
                    self.log.warn('No ssh config found for alias: %s' % ssh_config_name)
                    self.has_identity_file = False
        else:
            self.cfg = {
                'hostname': hostname,
                'username': username,
                'password': password,
                'port': int(port)
            }
            self.has_identity_file = False
        # create client
        self.client = paramiko.SSHClient()
        self.client.set_log_channel(self.paramiko_logger.name)
        self.client._policy = paramiko.WarningPolicy()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    def is_alive(self):
        """Tests if ssh connection is still alive"""

        # noinspection PyBroadException
        try:
            if self.transport and self.transport.is_active():
                self.transport.send_ignore()
                return True
            elif self.client.get_transport() and self.client.get_transport().is_active():
                self.client.get_transport().send_ignore()
                return True
            else:
                return False
        except Exception:
            # connection is closed
            return False

    def setup_rsa_key_authentification(self):
        """Automatically set up user private key and copy it to server"""

        # TODO implement setting up user private key and copy it to server

    def parse_ssh_config(self, ssh_config_file='~/.ssh/config'):
        """Parse the local ssh config file"""

        self.ssh_config_file = ssh_config_file
        self.client_config = paramiko.SSHConfig()

        user_config_file = os.path.expanduser(self.ssh_config_file)

        if os.path.exists(user_config_file):
            with open(user_config_file) as f:
                self.client_config.parse(f)
                return True
        return False

    def load_alias_config(self, ssh_config_name):
        """Load ssh config of given ssh alias"""

        self.ssh_config_name = ssh_config_name
        user_config = self.client_config.lookup(self.ssh_config_name)
        if 'hostname' in user_config:
            self.cfg['hostname'] = user_config['hostname']
        if 'user' in user_config:
            self.cfg['username'] = user_config['user']
        if 'port' in user_config:
            self.cfg['port'] = int(user_config['port'])
        if 'identityfile' in user_config:
            self.cfg['key_filename'] = user_config['identityfile'][0]
        return bool(user_config)

    def get_username(self):
        if 'username' in self.cfg.keys():
            return self.cfg['username']
        else:
            return None

    def exec(self, command):
        """Execute given <command> on cluster"""

        _timeout = self.TIMEOUT_EXECUTE
        self.TIMEOUT_EXECUTE = self.TIMEOUT_EXECUTE_DEFAULT

        self.log.debug('executing command: %s' % command)
        stdin, stdout, stderr = self.client.exec_command(command, timeout=_timeout)
        # Blocking call
        exit_code = stdout.channel.recv_exit_status()

        if exit_code == 0:
            self.log.debug('execution successful')
        else:
            self.log.warn('execution failed')

        stdin_read = ''
        stdout_read = ''
        stderr_read = ''
        # if not stdin.closed:
        #    stdin_read = stdin.readlines()
        if not stdout.closed:
            stdout_read = stdout.readlines()
        if not stderr.closed:
            stderr_read = stderr.readlines()
        # stdin.close()
        stdout.close()
        stderr.close()

        if self.log.isEnabledFor(10):
            stdout_msg = ''
            for line in stdout_read:
                stdout_msg += '... ' + line.strip('\n')
            self.log.debug(stdout_msg)

        return {'in': stdin_read, 'out': stdout_read, 'err': stderr_read}

    def connect(self, open_channel=False, interactive=False):
        """Open ssh connection"""

        _timeout = self.TIMEOUT_CONNECT
        self.TIMEOUT_CONNECT = self.TIMEOUT_CONNECT_DEFAULT

        if not self.is_alive():
            self.client.connect(**self.cfg, timeout=_timeout)
            if open_channel:
                port = 22
                if 'port' in self.cfg.keys():
                    port = self.cfg['port']
                # noinspection PyTypeChecker
                self.transport = paramiko.Transport((self.cfg['hostname'], port))
                if self.has_identity_file:
                    try:
                        rsa_key = paramiko.RSAKey.from_private_key_file(self.cfg['key_filename'])
                    except IOError:
                        rsa_key = None
                    except paramiko.PasswordRequiredException:
                        rsa_key = None
                    except paramiko.SSHException:
                        rsa_key = None
                    self.transport.connect(username=self.cfg['username'], pkey=rsa_key)
                else:
                    self.transport.connect(username=self.cfg['username'], password=self.cfg['password'])
                self.log.debug('[%s] Secure transport established' % self.cfg['hostname'])

                if interactive:
                    self.thread = threading.Thread(target=self.process)
                    self.thread.daemon = True
                    self.thread.start()
        if not self.shell:
            self.__open_shell()

    def disconnect(self):
        """Close ssh connection"""

        if self.client is not None:
            self.client.close()

        if self.transport is not None:
            self.transport.close()

    def __set_env_vars(self):
        var_list = []
        var_list += ['HISTCONTROL=ignoreboth']
        for var in var_list:
            self.send_shell('export %s' % var)

    def __open_shell(self):
        self.shell = self.client.invoke_shell()

        buffer = ''
        while not buffer.endswith("$ ") and not buffer.endswith("> "):
            buffer += str(self.shell.recv(1024), "utf8")
        self.log.debug("[%s] SSH login successful" % self.cfg['hostname'])

        # set environment variables
        self.__set_env_vars()
        self.log.debug('[%s] Environment variables set' % self.cfg['hostname'])

    def send_shell(self, command, no_history=True):

        _timeout = self.TIMEOUT_EXECUTE
        self.TIMEOUT_EXECUTE = self.TIMEOUT_EXECUTE_DEFAULT

        auto_connected = False
        if not self.is_alive():
            self.log.warn('[%s] No open connection, automatically establishing new connection' % self.cfg['hostname'])
            self.connect()
            auto_connected = True

        if not self.shell:
            self.log.debug('[%s] No open shell after connection, trying to re-open' % self.cfg['hostname'])
            self.__open_shell()

        self.shell.settimeout(_timeout)
        no_history_space = ' '
        if not no_history:
            no_history_space = ''
        self.log.debug("shell: %s" % command)
        self.shell.send("%s%s\n" % (no_history_space, command))
        buffer = ''
        while not buffer.endswith("$ ") and not buffer.endswith("> "):
            buffer += str(self.shell.recv(1024), "utf8")
        buffer = buffer.replace('\r', '')
        ind1 = buffer.find('\n')
        ind2 = buffer.rfind('\n')
        buffer_parsed = buffer[ind1 + 1:ind2]
        # self.log.debug('[stdout] %s' % buffer_parsed)
        self.shell.settimeout(self.TIMEOUT_EXECUTE_DEFAULT)

        if auto_connected:
            self.log.warn('[%s] Closing automatically established connection' % self.cfg['hostname'])
            self.disconnect()

        return buffer_parsed

    def sftp_open(self):
        self.sftp = paramiko.SFTPClient.from_transport(self.transport)

    def sftp_close(self):
        self.sftp.close()

    def sftp_put(self, local_file, remote_file):
        self.log.debug("scp: %s -> %s" % (local_file, remote_file))
        self.sftp.put(local_file, remote_file)

    def sftp_get(self, remote_file, local_file):
        self.log.debug("scp: %s <- %s" % (local_file, remote_file))
        self.sftp.get(remote_file, local_file)

    def get_file(self, remote_file, local_dest_folder,
                 compress=True, tmp_archive_folder='/tmp', delete_compressed=True):
        return self.get_path(remote_file, local_dest_folder, compress=compress, find=None,
                             tmp_archive_folder=tmp_archive_folder, delete_compressed=delete_compressed)

    def get_folder(self, remote_folder, local_dest_folder,
                   compress=True, tmp_archive_folder='/tmp', delete_compressed=True):
        return self.get_path(remote_folder, local_dest_folder, compress=compress, find=None,
                             tmp_archive_folder=tmp_archive_folder, delete_compressed=delete_compressed)

    def get_with_find(self, find_command, remote_basedir, local_dest_path, tmp_archive_folder,
                      delete_compressed=True):
        return self.get_path(remote_basedir, local_dest_path, compress=True, find=find_command,
                             tmp_archive_folder=tmp_archive_folder, delete_compressed=delete_compressed)

    def get_path(self, remote_path, local_dest_path, compress=True, find=None, tmp_archive_folder=None,
                 delete_compressed=True):
        """Retrieve a file from the server, optially compress is first before transfer"""

        remote_compressed = None
        if compress:
            random_str = ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(16)])
            remote_compressed = os.path.join(tmp_archive_folder, '%s.tar.gz' % random_str)
            if find:
                # self.send_shell('cd %s; %s -exec tar cfz %s --mode="a+rwX" {} +' % (remote_path, find, remote_compressed))
                self.send_shell('cd %s; tar cfz %s --mode="a+rwX" %s' %
                                (remote_path, remote_compressed, find))
            else:
                self.send_shell('tar cfz %s --mode="a+rwX" -C %s %s' %
                                (remote_compressed, os.path.dirname(remote_path), os.path.basename(remote_path)))
            local_dest = os.path.join(local_dest_path, os.path.basename(remote_compressed))
        else:
            local_dest = os.path.join(local_dest_path, os.path.basename(remote_path))

        self.sftp_open()
        if compress:
            self.sftp_get(remote_compressed, local_dest)
            self.send_shell('rm -f %s' % remote_compressed)
            os.chmod(local_dest, 0o775)
            # DEPRECATED: shutil.unpack_archive(local_dest, local_dest_path, format="gztar")
            tar = tarfile.open(local_dest, "r:gz")
            member = tar.next()
            member_base = None
            if member:
                member_base = member.name.split('/',1)[0].split(".",1)[0]
            tar.extractall(path=local_dest_path)
            tar.close()
            if member_base:
                for root, dirs, _ in os.walk(local_dest_path):
                    for d in dirs:
                        if d.startswith(member_base):
                            os.chmod(os.path.join(root, d), 0o775)
                    break
            if delete_compressed:
                os.remove(local_dest)
        else:
            self.sftp_get(remote_path, local_dest)
            os.chmod(local_dest, 0o775)
            for root, dirs, files in os.walk(local_dest):
                for d in dirs:
                    os.chmod(os.path.join(root, d), 0o775)
                for f in files:
                    os.chmod(os.path.join(root, f), 0o775)

        if compress:
            return local_dest, local_dest_path
        else:
            return None, local_dest

    def remove_dir(self, remote_path):
        self.send_shell('rm -rf %s' % remote_path)

    def remove_file(self, remote_path):
        self.send_shell('rm -f %s' % remote_path)

    def process(self):
        """Interactive shell input"""

        while True:
            # Print data when available
            if self.shell is not None and self.shell.recv_ready():
                alldata = self.shell.recv(1024)
                while self.shell.recv_ready():
                    alldata += self.shell.recv(1024)
                strdata = str(alldata, "utf8")
                strdata.replace('\r', '')
                print(strdata, end="")
                if strdata.endswith("$ "):
                    print("\n$ ", end="")

    def test_connection(self):
        self.connect()
        output = self.exec('hostname')
        for line in output['out']:
            self.log.info('[%s]> %s' % (self.ssh_config_name, line.strip('\n')))
        for line in output['err']:
            self.log.error('[%s]> %s' % (self.ssh_config_name, line.strip('\n')))
        self.disconnect()
