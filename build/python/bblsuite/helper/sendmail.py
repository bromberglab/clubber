#!/usr/bin/env python3
import smtplib
import os
from bblsuite.helper.config import Config
from email.utils import formataddr
from email.mime.text import MIMEText


__author__ = 'Yannick Mahlich'
__email__ = 'ymahlich@bromberglab.org'


"""
EXAMPLE CALL:


def send_email(template_fname, insvalues, toaddr,
               config=__CONFIG_PATH__):
    config = read_config(config)
    template_path = os.path.join(config['Sendmail'][services.template],
                                 template_fname)
    template = read_template(template_path)
    fromaddr = config['Sendmail']['services.email']
    realname = config['Sendmail']['services.realname']
    message = prep_mail(template, insvalues, toaddr, fromaddr, realname)
    send(message, toaddr, fromaddr, config)

"""


def read_config(conf_file=None):
    config = Config(config_file=conf_file).config
    return config


def read_template(template_fpath):
    with open(template_fpath, 'r') as template_file:
        template = template_file.read()
    return template


def prep_mail(template, insvalues, toaddr, fromaddr, realname):
    for key in insvalues:
        template = template.replace('<<{}>>'.format(key), insvalues[key])

    message = MIMEText(template)
    subject = "fusiondb mapper job with id '{}' sucessfully submitted".format(
        insvalues['id'])
    message['Subject'] = subject
    message['From'] = formataddr((realname, fromaddr))
    message['To'] = toaddr

    return message


def send(message, toaddr, fromaddr, config):
    server = smtplib.SMTP(host=config['Sendmail']['services.smtp'],
                          port=config['Sendmail']['services.port'])
    server.ehlo()
    server.starttls()
    server.login(config['Sendmail']['services.user'],
                 config['Sendmail']['services.password'])

    server.sendmail(fromaddr, [toaddr, 'services@bromberglab.org'],
                    message.as_string())
    server.quit()


def send_email(template_fname, insvalues, toaddr,
               config=None):
    config = read_config(config)
    template_path = os.path.join(config['Sendmail']['services.template'],
                                 template_fname)
    template = read_template(template_path)
    fromaddr = config['Sendmail']['services.email']
    realname = config['Sendmail']['services.realname']
    message = prep_mail(template, insvalues, toaddr, fromaddr, realname)
    send(message, toaddr, fromaddr, config)
