#!/bin/bash

# Python3
if command -v python3 > /dev/null 2>&1; then
    python="$(command -v python3)"
else
    echo "Python3 is required and could not be found in existing environment. Aborting."
    exit 1
fi

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize variables
library="python"
verbose=0

while getopts "h?vl:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    v)  verbose=1
        ;;
    l)  library=$OPTARG
        ;;
    esac
done

shift $((OPTIND -1))

[ "$1" = "--" ] && shift

# SCRIPT=$(readlink -f "$0") # linux only; workaround for osx&linux below:
CWD=`pwd -P`
TARGET_FILE="$0"
cd `dirname $TARGET_FILE`
TARGET_FILE=`basename $TARGET_FILE`

# Iterate down a (possible) chain of symlinks
while [ -L "$TARGET_FILE" ]
do
    TARGET_FILE=`readlink $TARGET_FILE`
    cd `dirname $TARGET_FILE`
    TARGET_FILE=`basename $TARGET_FILE`
done

# Compute the canonicalized name by finding the physical path
# for the directory we're in and appending the target file.
PHYS_DIR=`pwd -P`
SCRIPT=$PHYS_DIR/$TARGET_FILE
cd $CWD

BASEDIR=$(dirname "$SCRIPT")
export PYTHONPATH=$PYTHONPATH:$BASEDIR/:$BASEDIR/venv/lib/python3.6/site-packages/

if [[ $# -eq 0 ]]; then
    $python
elif [ $library = "python" ]; then
    cmd=`echo $@ | awk '{gsub(/\./,"/");printf $0}'`
    if [[ $verbose -eq 1 ]]; then
        echo "executing $python $BASEDIR/${cmd}.py"
    fi
    $python $BASEDIR/"$cmd".py
else
    echo "exec: $@"
    "$@"
fi

# End of file
