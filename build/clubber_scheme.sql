CREATE DATABASE clubber;

GRANT ALL ON *.* TO clubber@'%' IDENTIFIED BY 'clubber' WITH GRANT OPTION; FLUSH PRIVILEGES;

SET @@global.sql_mode="STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";

USE clubber;

CREATE TABLE projects
(
  id BIGINT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(50),
  description VARCHAR(200),
  user VARCHAR(50),
  script VARCHAR(100),
  sources VARCHAR(100),
  valid_clusters VARCHAR(100),
  transfer_dir VARCHAR(100),
  pre_processor VARCHAR(100),
  post_processor VARCHAR(100),
  dbstore TINYINT(1) DEFAULT '0' NOT NULL,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  UNIQUE (name)
) COMMENT='This table contains all registered projects' ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE project_jobs
(
  id BIGINT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  project BIGINT(11),
  status TINYINT(1) DEFAULT '0' NOT NULL,
  progress INT(10) NOT NULL DEFAULT '0',
  appending TINYINT(1) DEFAULT '0' NOT NULL,
  jobfile VARCHAR(200),
  args VARCHAR(500),
  submitted TIMESTAMP,
  started TIMESTAMP,
  finished TIMESTAMP,
  registered TIMESTAMP,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
) COMMENT='This table contains all sub projects jobs belonging to a certain project' ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE jobs
(
    id BIGINT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    project BIGINT(11),
    project_job BIGINT(11),
    daemon TINYINT(1) DEFAULT '0' NOT NULL,
    cluster VARCHAR(20),
    jobid MEDIUMINT(9) DEFAULT '0' NOT NULL,
    arrayidx SMALLINT(6) DEFAULT '0' NOT NULL,
    status TINYINT(1) DEFAULT '0' NOT NULL,
    submit TIMESTAMP,
    start TIMESTAMP,
    end TIMESTAMP,
    restarts TINYINT(1) DEFAULT '0',
    cmd VARCHAR(2000) NOT NULL,
    file VARCHAR(250),
    created TIMESTAMP NOT NULL,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
) COMMENT='This table contains all registered jobs' ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE results
(
    id BIGINT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    project BIGINT(11),
    project_job BIGINT(11),
    job BIGINT(11),
    bin_data LONGBLOB,
    txt_data LONGTEXT,
    filename VARCHAR(250),
    filesize VARCHAR(50),
    filetype VARCHAR(50),
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
) COMMENT='This table stores job results' ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
