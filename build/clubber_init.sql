USE clubber;

INSERT INTO clubber.users (name, email, password, api_token, remember_token, created_at, updated_at) VALUES ('Superadmin', 'admin@clubb.er', '$2y$10$hZ2eiEfu.ZSX50vILP36iufYbkFEo/15BjHiR6Fs.qGcH/0apuWEy', (SELECT SUBSTR(CONCAT(MD5(RAND()),MD5(RAND())),1,60)), null, NOW(), NOW());
INSERT INTO clubber.roles (name, guard_name, created_at, updated_at) VALUES ('Admin', 'web', NOW(), NOW());
INSERT INTO clubber.permissions (name, guard_name, created_at, updated_at) VALUES ('Administer roles & permissions', 'web', NOW(), NOW());
INSERT INTO clubber.role_has_permissions (permission_id, role_id) VALUES (1, 1);
INSERT INTO clubber.model_has_roles (role_id, model_id, model_type) VALUES (1, 1, 'App\\User');
INSERT INTO clubber.permissions (name, guard_name, created_at, updated_at) VALUES ('access project 1', 'web', NOW(), NOW());
