#!/bin/bash

sleep 3

script="/home/clubber/bromberglab_suite/run.sh"
newname="./clubber"

rm "$newname"

ln -s "$script" "$newname"

/sbin/setuser clubber "$newname" bblsuite/hpc/daemon.py start 2>&1

sleep 10
while [ -f /home/clubber/logs/daemon.pid ]
do
  sleep 10
done
