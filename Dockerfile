FROM phusion/baseimage
MAINTAINER Maximilian Miller <mmiller@bromberglab.org>

# ensure UTF-8
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# change resolv.conf
#RUN echo 'nameserver 8.8.8.8' > /etc/resolv.conf

# setup
ENV HOME /root
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

CMD ["/sbin/my_init"]

# nginx-php installation
RUN DEBIAN_FRONTEND="noninteractive" apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y upgrade
RUN DEBIAN_FRONTEND="noninteractive" apt-get update --fix-missing
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install php7.0
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install php7.0-fpm php7.0-common php7.0-cli php7.0-mysqlnd php7.0-mcrypt php7.0-curl php7.0-bcmath php7.0-mbstring php7.0-soap php7.0-xml php7.0-zip php7.0-json php7.0-imap php-xdebug php-pgsql
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install mysql-server
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install python3-dev libmysqlclient-dev
#RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install python3-setuptools
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install sudo
RUN DEBIAN_FRONTEND="noninteractive" apt-get install rsync

# add user and create directories
RUN useradd -m clubber
RUN mkdir -p /home/clubber/logs
RUN chown -R clubber:clubber /home/clubber/logs
RUN mkdir -p /home/clubber/wd
RUN chown -R clubber:clubber /home/clubber/wd

# append content to sudoers file
COPY build/sudoers.append /tmp/
RUN cat /tmp/sudoers.append >> /etc/sudoers && rm -f /tmp/sudoers.append

# install nginx (full)
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y nginx-full

# install php composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# add build script
RUN mkdir -p /root/setup
ADD build/setup.sh /root/setup/setup.sh
ADD build/clubber_scheme.sql /root/setup/clubber_scheme.sql
ADD build/clubber_init.sql /root/setup/clubber_init.sql
RUN chmod +x /root/setup/setup.sh
RUN (cd /root/setup/; /root/setup/setup.sh)

# copy files from repo
ADD build/nginx.conf /etc/nginx/sites-available/default
ADD build/.bashrc /root/.bashrc

# disable services start
RUN update-rc.d -f apache2 remove
RUN update-rc.d -f nginx remove
RUN update-rc.d -f php7.0-fpm remove
RUN update-rc.d -f mysql remove

# add startup scripts for nginx
ADD build/nginx.sh /etc/service/nginx/run
RUN chmod +x /etc/service/nginx/run

# add startup scripts for php7.0-fpm
ADD build/phpfpm.sh /etc/service/phpfpm/run
RUN chmod +x /etc/service/phpfpm/run

# add startup scripts for mysql
ADD build/mysql.sh /etc/service/mysql/run
RUN chmod +x /etc/service/mysql/run

# add startup scripts for clubber
ADD build/clubber.sh /etc/service/clubber/run
RUN chmod +x /etc/service/clubber/run

# set WWW public folder
#RUN mkdir -p /var/www/public
#ADD build/index.php /var/www/public/index.php

# install laravel
#RUN composer global require "laravel/installer"
#RUN composer create-project --prefer-dist laravel/laravel /var/www/clubber
#RUN composer require yajra/laravel-datatables-oracle
#RUN composer require laravelcollective/html

# switch user
# USER clubber

# copy clubber sources from repo
ADD build/web/clubber /var/www/clubber
RUN chown -R clubber:clubber /var/www/clubber
ADD build/python /home/clubber/bromberglab_suite/python
RUN chown -R clubber:clubber /home/clubber/bromberglab_suite

# clubber laravel database
RUN (service mysql start; mysql < /root/setup/clubber_scheme.sql; service mysql stop)
RUN (service mysql start; cd /var/www/clubber; php artisan migrate; service mysql stop)
RUN (service mysql start; mysql < /root/setup/clubber_init.sql; service mysql stop)

# switch user
USER root

# make www-data user owner of subfolders
RUN chown -R www-data:www-data /var/www/clubber/public/*
RUN chown -R www-data:www-data /var/www/clubber/storage/*
RUN chown -R www-data:www-data /var/www/clubber/bootstrap/cache/
RUN chown clubber:www-data /home/clubber/bromberglab_suite/python/config/main.conf
RUN chmod g+w /home/clubber/bromberglab_suite/python/config/main.conf

# set terminal environment
ENV TERM=xterm

# port and settings
EXPOSE 80 9000 3306

# cleanup apt and lists
RUN apt-get clean
RUN apt-get autoclean
